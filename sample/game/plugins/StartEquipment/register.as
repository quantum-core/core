void main() {
    auto mysql = core::GetMySQL();
    auto stmt = mysql.CreateStatement("SELECT * FROM `mysql`.`user`");
    auto result = stmt.Execute();
    while(result.Next()) {
        Info(result.GetString("User") + "%" + result.GetString("Host"));
    }

    command::RegisterCommand("test", TestCommand);
    command::RegisterCommand("level", SetLevelCommand);
}

void TestCommand(game::Player@ player, string[] arguments) {
    Info("Command test called!");
    if(arguments.length() < 2) {
        player.SendChatMessage(1, "/test [name]");
        return;
    }
    player.SendChatMessage(1, "Hello from " + player.name + "(" + player.level + ") to " + arguments[1]);
}

void SetLevelCommand(game::Player@ player, string[] arguments) {
    if(arguments.length() < 2) {
        player.SendChatMessage(1, "/level [level]");
        return;
    }

    auto level = parseInt(arguments[1]);
    if(level > 120 || level == 0) {
        player.SendChatMessage(1, "The level must between 1 and 120");
        return;
    }

    player.level = level;
}