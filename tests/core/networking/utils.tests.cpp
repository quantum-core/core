#include <core/networking/utils.hpp>

#include <gtest/gtest.h>

using namespace core::networking;

TEST(ConvertIPTest, ValidIPs) {
    ASSERT_EQ(ConvertIP("127.0.0.1"), 0x0100007f);
    ASSERT_EQ(ConvertIP("8.8.8.8"), 0x08080808);
}