QuantumCore
===========

**QuantumCore** is a reinvented open source implementation of the Metin2 server.

.. toctree::
   :maxdepth: 2

   building
   running
   scripting
   packets
   api/library_root