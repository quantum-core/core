Scripting
=========

QuantumCore is designed to be easily extendable using a scripting interface to allow
developers to build custom plugins.

Support languages
-----------------

+-------------+---------+---------------------------+
| Language    | Support | Documentation             |
+=============+=========+===========================+
| AngelScript | Good    | :doc:`/angelscript/index` |
+-------------+---------+---------------------------+

plugin.json
-----------

+-------+--------+-----------+---------------------------------------------------------------------+
| Field | Type   | Required? | Description                                                         |
+=======+========+===========+=====================================================================+
| name  | string | no        | Sets the name of this plugin                                        |
+-------+--------+-----------+---------------------------------------------------------------------+
| type  | string | yes       | Specify the used language for this plugin,                          |
|       |        |           | see Support languages                                               |
+-------+--------+-----------+---------------------------------------------------------------------+
| main  | string | no        | Specify the main script file to init the plugin.                    |
|       |        |           | For default value look at the corresponding language documentation. |
+-------+--------+-----------+---------------------------------------------------------------------+