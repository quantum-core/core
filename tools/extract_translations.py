import os
import sys
import re
from pathlib import Path
from xml.etree import ElementTree
import xml.dom.minidom as minidom

tr1 = r"[^a-zA-Z]TR\(((?<![\\])['\"])((?:.(?!(?<![\\])\1))*.?)\1"
tr2 = r"[^a-zA-Z]TRP\([a-zA-Z]+,\s*((?<![\\])['\"])((?:.(?!(?<![\\])\1))*.?)\1"

strings = {}


def get_next_id():
    with open('auto_increment.txt', 'r') as file:
        last_id = file.read()
        next_id = int(last_id) + 1

    with open('auto_increment.txt', 'w') as file:
        file.write(str(next_id))

    return str(next_id)


def process_match(file, line, match):
    path = str(Path(file).relative_to(sys.argv[1])).replace("\\", "/")
    string = match.group(2)

    if string in strings:
        strings[string]['sources'].append({
            'file': path,
            'line': line,
        })
    else:
        strings[string] = {
            'sources': [{
                'file': path,
                'line': line,
            }],
            'string': string,
        }


def work_file(path):
    with open(path, 'r') as file:
        data = file.read()

        matches = re.finditer(tr1, data, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            line = data[:match.start()].count("\n") + 1
            process_match(path, line, match)

        matches = re.finditer(tr2, data, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            line = data[:match.start()].count("\n") + 1
            process_match(path, line, match)


def work_directory(path):
    for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
        if filename.endswith(".cpp") or filename.endswith(".hpp"):
            work_file(file_path)
            continue
        if os.path.isdir(file_path):
            if filename.startswith("."):
                continue
            work_directory(file_path)
            continue


def write_references(source, unit):
    for i, ref in enumerate(strings[source]['sources']):
        group = ElementTree.SubElement(unit, '{urn:oasis:names:tc:xliff:document:1.2}context-group',
                                       {'name': 'po-reference', 'purpose': 'location'})
        fileElement = ElementTree.SubElement(group, '{urn:oasis:names:tc:xliff:document:1.2}context',
                                             {'context-type': 'sourcefile'})
        fileElement.text = ref['file']
        lineElement = ElementTree.SubElement(group, '{urn:oasis:names:tc:xliff:document:1.2}context',
                                             {'context-type': 'linenumber'})
        lineElement.text = str(ref['line'])


# Search for all translation strings in the codebase
work_directory(sys.argv[1])

# Parse current upstream version
tree = ElementTree.parse(os.path.join(sys.argv[1], 'sample', 'game', 'locale', 'en.xlf'))
locale = tree.getroot()

# Go through all current strings in locale and update the reference and maybe remove it if it's not referenced anymore
remove = []
for units in locale.findall(
        '{urn:oasis:names:tc:xliff:document:1.2}file/{urn:oasis:names:tc:xliff:document:1.2}body'):
    for unit in list(units):
        source = unit.find('{urn:oasis:names:tc:xliff:document:1.2}source').text
        if source in strings:
            # Refresh the references
            groups_remove = []
            for contextGroup in unit.iter('{urn:oasis:names:tc:xliff:document:1.2}context-group'):
                groups_remove.append(contextGroup)

            for group_remove in groups_remove:
                unit.remove(group_remove)

            write_references(source, unit)
            remove.append(source)
        else:
            units.remove(unit)

# Remove all strings which are already in the file
for i in remove:
    strings.pop(i)

# Go through all remaining strings
body = locale.find('{urn:oasis:names:tc:xliff:document:1.2}file/{urn:oasis:names:tc:xliff:document:1.2}body')
for string in strings:
    transUnit = ElementTree.SubElement(body, '{urn:oasis:names:tc:xliff:document:1.2}trans-unit',
                                       {'approved': 'yes', 'id': get_next_id()})
    source = ElementTree.SubElement(transUnit, '{urn:oasis:names:tc:xliff:document:1.2}source')
    source.text = string

    write_references(string, transUnit)

ElementTree.register_namespace('', 'urn:oasis:names:tc:xliff:document:1.2')
xml_string = '\n'.join(
    [line for line in
     minidom.parseString(ElementTree.tostring(locale)).toprettyxml(indent='    ').split('\n') if
     line.strip()])
# tree.write(os.path.join(sys.argv[1], 'sample', 'game', 'locale', 'en_updated.xlf'), encoding='UTF-8',
#           xml_declaration=True)
with open(os.path.join(sys.argv[1], 'sample', 'game', 'locale', 'en.xlf'), "wb") as file:
    file.write(xml_string.encode("UTF-8"))

print(strings)
