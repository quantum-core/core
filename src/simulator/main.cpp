﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "application.hpp"
#include "boost/program_options.hpp"
#include "exit_codes.hpp"

namespace po = boost::program_options;

int main(int ac, char **av) {
    po::options_description desc("Allowed options");
    desc.add_options()(
        "help,h", "produce help message")(
        "username,u", po::value<std::string>(),
            "username the bot will use for login")(
        "password,p", po::value<std::string>(),
            "password the bot will use for login")(
        "default_char", po::value<uint8_t>()->default_value(uint8_t(0), "0"),
            "the bot will select the character on this position by default")(
        "auth_ip", po::value<std::string>()->default_value("127.0.0.1"),
            "the bot will use this address for connecting to the auth")(
        "game_ip", po::value<std::string>()->default_value("127.0.0.1"),
            "the bot will use this address for connecting to the game")(
        "auth_port", po::value<uint16_t>()->default_value(11002),
            "the bot will use this port for connecting to the auth")(
        "game_port", po::value<uint16_t>()->default_value(13001),
            "the bot will use this port for connecting to the game")(
        "reconnect_if_failed", po::value<bool>()->default_value(true),
            "the bot will try to reconnect to the server if the "
            "connection to the target server was not successful")(
        "try_reconnect", po::value<bool>()->default_value(true),
            "the bot will try to reconnect to the game if the game has closed the "
            "session");

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(ac, av)
                                    .options(desc)
                                    .allow_unregistered()
                                    .run();
    po::store(parsed, vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return simulator::ErrorCodes::ERROR_CHECK_HELP;
    }

    std::vector<std::string> unrecognizedOptions =
        po::collect_unrecognized(parsed.options, po::include_positional);

    {
        std::shared_ptr<simulator::Application> app =
            std::make_shared<simulator::Application>();
        int optionsSet = app->SetOptions(vm, unrecognizedOptions);
        if (optionsSet) return optionsSet;
        app->Start();
    }
    system("pause");
    return simulator::ErrorCodes::ERROR_NONE;
}
