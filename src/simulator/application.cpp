// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "application.hpp"

#include <libbcrypt.h>

#include <utility>
#include "networking/client_connector.hpp"
#include "networking/session.hpp"
#include <csignal>
#include <boost/bind.hpp>

#include "entity/entity_manager.hpp"
#include "simulations/simulation.hpp"

#include "exit_codes.hpp"

const uint32_t UPDATE_TICK_RATE = 10;

namespace simulator {
    std::shared_ptr<Application> Application::_instance = nullptr;

    Application::Application()
        : _networkThread(), _context(), _loginKey(0U), _serverTime(0) {
        CORE_LOGGING(trace) << "Creating application";
        _bootTime = boost::posix_time::microsec_clock::local_time();
        _entityManager = std::make_shared<entity::EntityManager>();
    }
    Application::~Application() {
        CORE_LOGGING(trace) << "Destroying application";
        _connector->SetDisconnectHandler(nullptr);
        _connector->SetConnectionFailedHandler(nullptr);
    }

    volatile sig_atomic_t stop;

    void signal_handler(int signal) {
        CORE_LOGGING(trace) << "Signal handled";
        stop = signal;
    }

    void Application::Start() {
        assert(_instance == nullptr);
        _instance = shared_from_this();

        _entityManager->Clear();
        _connector = std::make_shared<networking::ClientConnector>(
            shared_from_this(), _context, 3,
            reinterpret_cast<const uint32_t *>("testtesttesttest"));

        std::signal(SIGINT, signal_handler);
        std::signal(SIGTERM, signal_handler);

        _connector->ConnectTo(_botOptions.authIP, _botOptions.authPort,
                              networking::HandlerType::HANDLER_AUTH);
        PrepareHandlers();

        _networkThread = std::thread{[this]() { _context.run(); }};
        auto lastTime = GetCoreTime();
        uint32_t accumulator = 0;

        while (!stop) {
            auto current = GetCoreTime();
            auto elapsedTime = current - lastTime;
            lastTime = current;

            accumulator += elapsedTime;
            while (accumulator >= UPDATE_TICK_RATE) {
                Update(UPDATE_TICK_RATE);
                accumulator -= UPDATE_TICK_RATE;
            }

            auto updateTime = GetCoreTime() - current;
            if (updateTime < UPDATE_TICK_RATE) {
                std::this_thread::sleep_for(
                    std::chrono::milliseconds(UPDATE_TICK_RATE - updateTime));
            }
        }

        // Request asio to stop the event processing loop
        _context.stop();
        // Wait for network thread to end
        _networkThread.join();
        // disconnect the client and wait for the network to finalize it
        _connector->Shutdown();
        _context.restart();
        _instance = nullptr;
    }

    uint32_t Application::GetCoreTime() {
        auto now = boost::posix_time::microsec_clock::local_time();
        auto diff = now - _bootTime;

        return diff.total_milliseconds();
    }

    uint32_t Application::GetServerTime() { 
        return GetCoreTime() + _serverTime;
    }

    std::shared_ptr<networking::ClientConnector> Application::GetConnector() {
        return _connector;
    }

    int Application::SetOptions(boost::program_options::variables_map &vm,
                                std::vector<std::string> &unrecognizedOptions) {
        for (auto it : unrecognizedOptions) {
            CORE_LOGGING(warning) << "Unrecognized option: " << it;
        }

        if (!vm.count("username")) {
            CORE_LOGGING(error) << "Username is not set in the arguments";
            return ErrorCodes::ERROR_USERNAME_NOT_SET;
        }
        _botOptions.userName = vm["username"].as<std::string>();
        CORE_LOGGING(trace)
            << "Current username is " << _botOptions.userName;

        if (!vm.count("password")) {
            CORE_LOGGING(error) << "Password is not set in the arguments";
            return ErrorCodes::ERROR_PASSWORD_NOT_SET;
        }
        _botOptions.password = vm["password"].as<std::string>();
        _botOptions.defaultChar = vm["default_char"].as<uint8_t>();

        _botOptions.authIP = vm["auth_ip"].as<std::string>();
        _botOptions.gameIP = vm["game_ip"].as<std::string>();
        _botOptions.authPort = vm["auth_port"].as<uint16_t>();
        _botOptions.gamePort = vm["game_port"].as<uint16_t>();
        _botOptions.reconnectIfFailed = vm["reconnect_if_failed"].as<bool>();
        _botOptions.tryReconnect = vm["try_reconnect"].as<bool>();

        _simulation = std::make_shared<simulations::Simulation>();
        _simulation->Load();
        return ErrorCodes::ERROR_NONE;
    }

    void Application::Update(uint32_t elapsedTime) {
        _entityManager->Update(elapsedTime);
    }

    void Application::PrepareHandlers() {
        _connector->SetDisconnectHandler(
            boost::bind(&Application::OnDisconnect, this));
        _connector->SetConnectionFailedHandler(
            boost::bind(&Application::OnFailedToConnect, this));
    }

    void Application::OnDisconnect() {
        if (!_botOptions.tryReconnect) return;
        if (stop) {
            CORE_LOGGING(info)
                << "[OnDisconnect] Program is shutting down, no reconnection";
            return;
        }

        // this will make sure that the update will not try to interfere with
        // sends while we are not connected
        _entityManager->Clear();
        if (_loginKey) {
            CORE_LOGGING(trace)
                << "[OnDisconnect] Auth key is set, logging into core";
            _connector->ConnectTo(_botOptions.gameIP, _botOptions.gamePort,
                                  networking::HandlerType::HANDLER_GAME);
        } else {
            CORE_LOGGING(trace)
                << "[OnDisconnect] Auth key is not set, connecting to auth";
            _connector->ConnectTo(_botOptions.authIP, _botOptions.authPort,
                                  networking::HandlerType::HANDLER_AUTH);
        }
        PrepareHandlers();
    }

    void Application::OnFailedToConnect() { 
        _loginKey = 0;
        if (!_botOptions.reconnectIfFailed) return;

        bool saveOption = _botOptions.tryReconnect;
        _botOptions.tryReconnect = true;
        OnDisconnect();
        _botOptions.tryReconnect = saveOption;
    }

    boost::asio::io_context &Application::GetContext() { return _context; }
    std::shared_ptr<Application> Application::GetInstance() {
        assert(_instance != nullptr);
        return _instance;
    }

} // namespace auth
