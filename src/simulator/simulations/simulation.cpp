// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "simulation.hpp"
#include "../application.hpp"
#include "../entity/entity_manager.hpp"
#include "../exit_codes.hpp"

namespace simulator {
    namespace simulations {
        Simulation::Simulation() { 
        }

        void Simulation::Load() { LoadSimulation(); }

        bool Simulation::MovementComplete(bool isMain, int32_t vid,
                                          int32_t fromX, int32_t fromY) {
            if (!isMain) return true;
            std::lock_guard<std::mutex> guard(_movementMutex);
            if (!_movements.HasTask()) return true;
            _movements();
            return false;
        }

        void Simulation::OnMainInstanceLoad() {
            std::lock_guard<std::mutex> guard(_movementMutex);
            if (!_movements.HasTask()) return;
            _movements();
        }

        void Simulation::LoadSimulation() {
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 333872, 755519));
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 331357, 753124));
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 334839, 753191));
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 333872, 755519));
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 334839, 753191));
            _movements.LoadTask(std::bind(&Simulation::MoveTo,
                                          shared_from_this(), 331357, 753124));
        }

        void Simulation::MoveTo(int32_t x, int32_t y) {
            auto mainChar = Application::GetInstance()->GetEntityManager()->GetMainCharacter();
            if (!mainChar) {
                CORE_LOGGING(error)
                    << "Unplanned error, main char not found while moving to "
                    << std::to_string(x) << ", " << std::to_string(y);
                std::exit(ErrorCodes::SIMULATION_ERROR_NO_MAIN_CHAR_WHILE_MOVE);
            }
            mainChar->MoveTo(x, y);
        }
    }  // namespace simulations
}