// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "task.hpp"
#include <memory>
#include <mutex>

namespace simulator {
    namespace entity {
        class Entity;
    }
    namespace simulations {
        class Simulation : public std::enable_shared_from_this<Simulation> {
        public:
            Simulation();
            virtual ~Simulation() = default;
            void Load();

            bool MovementComplete(bool isMain, int32_t vid, int32_t fromX,
                                  int32_t fromY);
            void OnMainInstanceLoad();

        private:
            void LoadSimulation();

            void MoveTo(int32_t x, int32_t y);

        private:
            std::mutex _movementMutex;
            Task<void> _movements;
        };
    }
}