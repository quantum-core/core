// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once
#include <cstdint>
#include <functional>
#include <queue>
#include <atomic>

namespace simulator {
    namespace simulations {
        template <class ReturnType>
        class Task {
            typedef std::function<ReturnType()> HandleType;
        public:
            Task() : _repeat(true), _runOnUpdate(false) {}
            Task(bool repeat, bool runOnUpdate)
             : _repeat(repeat), _runOnUpdate(runOnUpdate) {}

            void SetRepeat(bool val) { _repeat = val; }
            void SetRunOnUpdate(bool val) { _runOnUpdate = val; }
            void LoadTask(HandleType fnc) { 
                _taskQueue.push(fnc);
            }

            ReturnType Update(uint32_t elapsedTime) {
                if (!_runOnUpdate) return;
                return operator()();
            }

            bool HasTask() const { return !_taskQueue.empty(); }
            bool IsRepeat() const { return _repeat; }
            bool IsRunOnUpdate() const { return _runOnUpdate; }
            
            ReturnType operator()() { 
                HandleType task = _taskQueue.front();
                _taskQueue.pop();
                ReturnType result = task();
                if (_repeat) {
                    _taskQueue.push(task);
                }
                return result;
            }

        private:
            std::atomic<bool> _repeat, _runOnUpdate;
            std::queue<HandleType> _taskQueue;
        };

        template <>
        class Task<void> {
            typedef std::function<void()> HandleType;

        public:
            Task() : _repeat(true), _runOnUpdate(false) {}
            Task(bool repeat, bool runOnUpdate)
                : _repeat(repeat), _runOnUpdate(runOnUpdate) {}

            void SetRepeat(bool val) { _repeat = val; }
            void SetRunOnUpdate(bool val) { _runOnUpdate = val; }
            void LoadTask(HandleType fnc) { _taskQueue.push(fnc); }

            bool HasTask() const { return !_taskQueue.empty(); }
            bool IsRepeat() const { return _repeat; }
            bool IsRunOnUpdate() const { return _runOnUpdate; }

            void Update(uint32_t elapsedTime) {
                if (!_runOnUpdate) return;
                operator()();
            }

            void operator()() {
                HandleType task = _taskQueue.front();
                _taskQueue.pop();
                task();
                if (_repeat) {
                    _taskQueue.push(task);
                }
            }

        private:
            std::atomic<bool> _repeat, _runOnUpdate;
            std::queue<HandleType> _taskQueue;
        };
    }  // namespace simulations
}  // namespace simulator
