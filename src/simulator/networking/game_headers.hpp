// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

namespace simulator {
    // login phase packets
    enum OutgoingGameLoginHeaders : uint8_t {
        HEADER_OUT_LOGIN = 0x6d,
        HEADER_OUT_EMPIRE = 0x5a,
    };

    enum IncomingGameLoginHeaders : uint8_t {
        HEADER_INC_EMPIRE = 0x5a,
    };

    // select phase packets
    enum OutgoingGameSelectHeaders : uint8_t {
        HEADER_OUT_SELECT_SLOT = 0x06,
    };

    enum IncomingGameSelectHeaders : uint8_t {
        HEADER_INC_CHARACTERS = 0x20,
    };

    // loading phase packets
    enum OutgoingGameLoadingHeaders : uint8_t {
        HEADER_OUT_ENTER_GAME = 0x0a,
    };

    enum IncommingGameLoadingHeaders : uint8_t {
        HEADER_INC_CHARACTER_DETAILS = 0x71,
        HEADER_INC_CHARACTER_POINTS = 0x10,
    };

    // game phase packets
    enum OutgoingGameHeaders : uint8_t { 
        HEADER_OUT_CHAT = 0x03,
		HEADER_OUT_MOVE = 0x07,
    };

    enum IncomingGameHeaders : uint8_t {
        HEADER_INC_TIME_SYNC = 0x6a,
        HEADER_INC_CHANNEL_ID = 0x79,
        HEADER_INC_ADD_CHARACTER = 0x01,
        HEADER_INC_CHARACTER_INFO = 0x88,
        HEADER_INC_CHAT = 0x04,
        HEADER_INC_CHARACTER_MOVE = 0x03,
        HEADER_INC_REMOVE_CHARACTER = 0x02,
        HEADER_INC_SET_ITEM = 0x15,
    };

} // namespace simulator
