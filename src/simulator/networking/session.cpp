﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "session.hpp"

#include "../../core/networking/cryptation.hpp"
#include "reserved_headers.hpp"
#include "client_connector.hpp"
#include "packets/default_packets.hpp"
#include "../application.hpp"
#include "../entity/entity_manager.hpp"

namespace simulator::networking {
    SessionBase::SessionBase(std::shared_ptr<Application> app,
                                   std::shared_ptr<ClientConnector> connector)
        : _application(std::move(app)),
          _connector(std::move(connector)),
          _phase(PHASE_OFFLINE),
          _handshake(0),
          _handshaking(false) {
        CORE_LOGGING(trace) << "Creating base session";
    }

    SessionBase::~SessionBase() {
        CORE_LOGGING(trace) << "Destroying base session";
    }

    void SessionBase::RegisterHandlers() {
        CORE_LOGGING(trace) << "Registering handlers for base session";

        auto connector = _connector.lock();
        if (!connector) {
            CORE_LOGGING(error) << "Invalid weakref for connector";
            return;
        }

        RegisterDefaultPackets();

        connector->RegisterHandler(
            ReservedIncomingHeaders::HEADER_INC_HANDSHAKE,
            std::bind(&SessionBase::HandleHandshake, shared_from_this(),
                      std::placeholders::_1));

        if (connector->GetSecurityLevel() > SECURITY_LEVEL_XTEA) {
            connector->RegisterHandler(
                ReservedIncomingHeaders::HEADER_INC_KEY_AGREEMENT,
                std::bind(&SessionBase::HandleKeyAgreement, shared_from_this(),
                          std::placeholders::_1));

            connector->RegisterHandler(
                ReservedIncomingHeaders::HEADER_INC_KEY_AGREEMENT_COMPLETED,
                std::bind(&SessionBase::HandleKeyAgreementCompleted,
                          shared_from_this(), std::placeholders::_1));
        }

        connector->RegisterHandler(
            ReservedIncomingHeaders::HEADER_INC_PHASE,
            std::bind(&SessionBase::HandlePhase, shared_from_this(),
                      std::placeholders::_1));
    }

    void SessionBase::HandlePhase(
        std::shared_ptr<core::networking::Packet> packet) {
        CORE_LOGGING(trace) << "Recv phase packet ";

        auto oldPhase = _phase;
        _phase = packet->GetField<uint8_t>("phase");
        if (_phase == PHASE_HANDSHAKE) {
            _handshaking = true;
        }

        // fix to make it work with the workaround for real clients
        if (oldPhase == PHASE_HANDSHAKE) {
            _connector.lock()->GetCryptation()->Activate();
        } else if (oldPhase == PHASE_GAME) {
            Application::GetInstance()->GetEntityManager()->Clear();
        }

        CORE_LOGGING(trace) << "Phase changed to " << std::to_string(_phase);
        OnPhaseChange(oldPhase);
    }

    void SessionBase::HandleKeyAgreementCompleted(
        std::shared_ptr<core::networking::Packet> packet) {
        CORE_LOGGING(trace) << "Recv key agreement completed packet";

        auto connector = _connector.lock();
        auto cryptation = connector->GetCryptation();
        if (!cryptation) {
            CORE_LOGGING(error) << "No cryptation!";
            connector->Close();
            return;
        }
        if (!cryptation->IsInitialized()) {
            CORE_LOGGING(error) << "Cryptation is not initialized!";
            connector->Close();
            return;
        }
        // fix to make it work with the workaround for real clients
        //cryptation->Activate();
        _handshaking = false;
    }

    void SessionBase::SendHandshake(uint32_t time, uint32_t delta) {
        CORE_LOGGING(trace) << "Send handshake packet";

        auto connector = _connector.lock();
        auto packet = connector->GetPacketManager().lock()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_HANDSHAKE,
            core::networking::Direction::Outgoing);
        packet->SetField<uint32_t>("handshake", _handshake);
        packet->SetField<uint32_t>("time", time);
        packet->SetField<uint32_t>("delta", 0);

        connector->Send(packet);
    }

    void SessionBase::RegisterDefaultPackets(bool onlyPhase) {
        auto connector = _connector.lock();
        if (!connector) {
            CORE_LOGGING(error) << "Invalid weakref for connector";
            return;
        }

        if (!onlyPhase)
            packets::RegisterDefault_packets(
                connector->GetPacketManager().lock());
        else
            packets::RegisterPacketPhase(
                connector->GetPacketManager().lock());
    }

    void SessionBase::HandleHandshake(std::shared_ptr<core::networking::Packet> packet) {
        CORE_LOGGING(trace) << "Recv handshake packet";

        _handshake = packet->GetField<uint32_t>("handshake");
        auto time = packet->GetField<uint32_t>("time");
        auto delta = packet->GetField<uint32_t>("delta");

        Application::GetInstance()->SetServerTime(time + delta);

        SendHandshake(time + delta * 2, 0);
    }

    void SessionBase::HandleKeyAgreement(
        std::shared_ptr<core::networking::Packet> packet) {
        CORE_LOGGING(trace) << "Recv key agreement packet";

        auto connector = _connector.lock();
        auto cryptation = connector->GetCryptation();
        if (!cryptation) {
            CORE_LOGGING(error) << "No cryptation!";
            connector->Close();
            return;
        }

        auto valueLen = packet->GetField<uint16_t>("valuelen");
        auto dataLen = packet->GetField<uint16_t>("datalen");
        auto data = (uint8_t *)packet->GetField("data");

        if (cryptation->IsInitialized()) {
            CORE_LOGGING(error) << "Cryptation is already initialized!";
            connector->Close();
            return;
        }

        if (!cryptation->Initialize()) {
            CORE_LOGGING(error) << "Failed to initialize cryptation!";
            connector->Close();
            return;
        }

        const CryptoPP::SecByteBlock *staticKey =
            (const CryptoPP::SecByteBlock *)(cryptation->GetData(
                core::networking::KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY));
        const CryptoPP::SecByteBlock *ephemeralKey =
            (const CryptoPP::SecByteBlock *)(cryptation->GetData(
                core::networking::
                    KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY));

        const uint32_t *agreedValue = (const uint32_t *)cryptation->GetData(
            core::networking::KEY_AGREEMENT_DATA_AGREED_VALUE);
        if (*agreedValue < 1 || staticKey->size() < 1 ||
            ephemeralKey->size() < 1 ||
            (ephemeralKey->size() + staticKey->size() > 256)) {
            CORE_LOGGING(error) << "Invalid cryptation key values!";
            connector->Close();
            return;
        }

        cryptation->AddData(core::networking::KEY_AGREEMENT_DATA_AGREED_VALUE,
                            &valueLen, sizeof(valueLen));
        cryptation->AddData(
            core::networking::KEY_AGREEMENT_DATA_PUBLIC_STATIC_CLIENT_KEY, data,
            staticKey->size());
        cryptation->AddData(
            core::networking::KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_CLIENT_KEY,
            data + staticKey->size(), ephemeralKey->size());
        bool invert = true;
        cryptation->AddData(core::networking::KEY_AGREEMENT_DATA_INVERT_CIPHER,
                            &invert, sizeof(invert));

        if (!cryptation->Finalize()) {
            CORE_LOGGING(error) << "Failed to finalize cryptation!";
            connector->Close();
            return;
        }

        char keys[256];
        memset(keys, 0, sizeof(keys));
        memcpy(keys, staticKey->data(), staticKey->size());
        memcpy(keys + staticKey->size(), ephemeralKey->data(),
               ephemeralKey->size());

        auto outPacket = connector->GetPacketManager().lock()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_KEY_AGREEMENT,
            core::networking::Direction::Outgoing);
        outPacket->SetField<uint16_t>("valuelen",
                                   static_cast<uint16_t>(*agreedValue));
        outPacket->SetField<uint16_t>(
            "datalen",
            static_cast<uint16_t>(ephemeralKey->size() + staticKey->size()));
        outPacket->SetField("data", (uint8_t *)keys, sizeof(keys));

        connector->Send(outPacket);
    }

    uint8_t SessionBase::GetType() { return HandlerType::HANDLER_NONE; }
}  // namespace simulator::networking
