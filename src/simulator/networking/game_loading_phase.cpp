// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "../application.hpp"
#include "client_connector.hpp"
#include "game_headers.hpp"
#include "game_session.hpp"
#include "packets/game_loading_packets.hpp"

namespace simulator {
    namespace networking {
        void GameSession::RegisterLoadingPhaseHandlers() {
            auto connector = _connector.lock();

            connector->RegisterHandler(
                IncommingGameLoadingHeaders::HEADER_INC_CHARACTER_DETAILS,
                std::bind(&GameSession::HandleCharacterDetailsLoading,
                          shared_from_this(), std::placeholders::_1));
            connector->RegisterHandler(
                IncommingGameLoadingHeaders::HEADER_INC_CHARACTER_POINTS,
                std::bind(&GameSession::HandleCharacterPointsLoading,
                          shared_from_this(), std::placeholders::_1));
        }

        void GameSession::HandleCharacterDetailsLoading(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive character details packet";

            //whatever
        }

        void GameSession::HandleCharacterPointsLoading(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive character points packet";

            // whatever

            CORE_LOGGING(trace) << "Send entergame packet";
            auto connector = _connector.lock();
            auto outPacket = connector->GetPacketManager().lock()->CreatePacket(
                OutgoingGameLoadingHeaders::HEADER_OUT_ENTER_GAME,
                core::networking::Direction::Outgoing);

            connector->Send(outPacket);
        }

        void GameSession::HandleLoadingPhase(uint8_t oldPhase) {
            auto connector = _connector.lock();
            auto packetManager = connector->GetPacketManager().lock();
            packetManager->Clear();
            RegisterDefaultPackets(true);
            packets::RegisterGame_loading_packets(packetManager);
        }
    }  // namespace networking
}  // namespace simulator