// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "../application.hpp"
#include "client_connector.hpp"
#include "game_headers.hpp"
#include "game_session.hpp"
#include "packets/game_select_packets.hpp"

namespace simulator {
    namespace networking {
        void GameSession::RegisterSelectPhaseHandlers() {
            auto connector = _connector.lock();

            connector->RegisterHandler(
                IncomingGameSelectHeaders::HEADER_INC_CHARACTERS,
                std::bind(&GameSession::HandleCharactersPacketLogin,
                          shared_from_this(), std::placeholders::_1));
        }

        void GameSession::HandleCharactersPacketLogin(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive characters packet";
            // whatever

            CORE_LOGGING(trace) << "Send character select packet";
            auto connector = _connector.lock();
            auto outPacket = connector->GetPacketManager().lock()->CreatePacket(
                OutgoingGameSelectHeaders::HEADER_OUT_SELECT_SLOT,
                core::networking::Direction::Outgoing);

            outPacket->SetField<uint8_t>("slot", 0);

            connector->Send(outPacket);
        }

        void GameSession::HandleSelectPhase(uint8_t oldPhase) {
            auto connector = _connector.lock();
            auto packetManager = connector->GetPacketManager().lock();
            packetManager->Clear();
            RegisterDefaultPackets(true);
            packets::RegisterGame_select_packets(packetManager);
        }
    }  // namespace networking
}  // namespace simulator