// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "../application.hpp"
#include "client_connector.hpp"
#include "game_headers.hpp"
#include "game_session.hpp"
#include "packets/game_packets.hpp"

#include "../entity/entity_manager.hpp"
#include "../entity/entity_player.hpp"

namespace simulator {
    namespace networking {
        void GameSession::RegisterGamePhaseHandlers() {
            auto connector = _connector.lock();

            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_TIME_SYNC,
                std::bind(&GameSession::HandleTimeSyncGame, shared_from_this(),
                          std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_CHANNEL_ID,
                std::bind(&GameSession::HandleChannelIDGame, shared_from_this(),
                          std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_ADD_CHARACTER,
                std::bind(&GameSession::HandleAddCharacterGame,
                          shared_from_this(),
                          std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_CHARACTER_INFO,
                std::bind(&GameSession::HandleCharacterInfoGame,
                          shared_from_this(), std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_CHAT,
                std::bind(&GameSession::HandleChatGame,
                          shared_from_this(), std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_CHARACTER_MOVE,
                std::bind(&GameSession::HandleCharacterMoveGame,
                          shared_from_this(), std::placeholders::_1));
            connector->RegisterHandler(
                IncomingGameHeaders::HEADER_INC_REMOVE_CHARACTER,
                std::bind(&GameSession::HandleRemoveCharacterGame,
                          shared_from_this(), std::placeholders::_1));
        }

        void GameSession::HandleTimeSyncGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive time sync packet";

            // whatever
        }

        void GameSession::HandleChannelIDGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive time sync packet";
        }

        void GameSession::HandleAddCharacterGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive add character packet";
            auto manager = _application.lock()->GetEntityManager();
            manager->CreateEntity(packet->GetField<uint32_t>("vid"),
                                  packet->GetField<uint8_t>("characterType"),
                                  packet->GetField<int32_t>("x"),
                                  packet->GetField<int32_t>("y"),
                                  packet->GetField<float>("angle"),
                                  packet->GetField<uint16_t>("class"),
                                  packet->GetField<uint8_t>("state"),
                                  packet->GetField<uint8_t>("moveSpeed"),
                                  packet->GetField<uint8_t>("attackSpeed"));
        }

        void GameSession::HandleCharacterInfoGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive character info packet";
            auto charName = packet->GetString("name");
            auto manager = _application.lock()->GetEntityManager();
            auto player = manager->GetPlayer(packet->GetField<uint32_t>("vid"));
            player->SetName(charName);

            std::string answer = "Hello " + charName;
            SendChatMessage(answer, 0);
            manager->SetMainCharacter(packet->GetField<uint32_t>("vid"));
        }

        void GameSession::HandleChatGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace)
                << "Receive chat packet: " << packet->GetDynamicString();
        }

        void GameSession::HandleCharacterMoveGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive character move packet";
        }

        void GameSession::HandleRemoveCharacterGame(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive character remove packet";
            auto manager = _application.lock()->GetEntityManager();
            auto vid = packet->GetField<uint32_t>("vid");

            auto entity = manager->GetEntity(vid);
            if(entity->GetType() == entity::Entity::EntityType::ENTITY_PLAYER) {
                auto player = std::static_pointer_cast<entity::EntityPlayer>(entity);
                std::string charName;
                manager->GetPlayer(vid)->GetName(charName);
                charName = "Bye " + charName;
                SendChatMessage(charName, 0);
            }

            manager->RemoveEntity(vid);
        }

        // Game phase functions
        void GameSession::SendChatMessage(const std::string& message,
                                          uint8_t chatMessageType) {
            auto connector = _connector.lock();
            auto outPacket = connector->GetPacketManager().lock()->CreatePacket(
                OutgoingGameHeaders::HEADER_OUT_CHAT,
                core::networking::Direction::Outgoing);

            outPacket->SetField<uint8_t>("messageType", chatMessageType);
            outPacket->SetDynamicString(message);
            connector->Send(outPacket);
        }

        void GameSession::HandleGamePhase(uint8_t oldPhase) {
            auto connector = _connector.lock();
            auto packetManager = connector->GetPacketManager().lock();
            packetManager->Clear();
            RegisterDefaultPackets(true);
            packets::RegisterGame_packets(packetManager);
        }
    }  // namespace networking
}  // namespace simulator