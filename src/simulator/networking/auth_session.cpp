﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "auth_session.hpp"

#include "../../core/networking/cryptation.hpp"
#include "auth_headers.hpp"
#include "packets/auth_packets.hpp"
#include "client_connector.hpp"
#include "../application.hpp"
#include "reserved_headers.hpp"

namespace simulator::networking {
    AuthSession::AuthSession(
        std::shared_ptr<Application> app,
        std::shared_ptr<ClientConnector> connector)
        : SessionBase(app, connector) {}

    void AuthSession::RegisterHandlers() {
        SessionBase::RegisterHandlers();

        CORE_LOGGING(trace) << "Registering handlers for auth session";
        auto connector = _connector.lock();

        connector->RegisterHandler(
            IncomingAuthHeaders::HEADER_INC_LOGIN_SUCCESS,
            std::bind(&AuthSession::HandleLoginSuccess, shared_from_this(),
                      std::placeholders::_1));
        connector->RegisterHandler(
            IncomingAuthHeaders::HEADER_INC_LOGIN_FAILED,
            std::bind(&AuthSession::HandleLoginFailed,
                      shared_from_this(), std::placeholders::_1));
    }

    uint8_t AuthSession::GetType() { return HandlerType::HANDLER_AUTH; }

    void AuthSession::OnPhaseChange(uint8_t oldPhase) {
        if (_phase == Phases::PHASE_AUTH) {
            auto connector = _connector.lock();
            auto app = _application.lock();
            packets::RegisterAuth_packets(connector->GetPacketManager().lock());

            auto packet = connector->GetPacketManager().lock()->CreatePacket(
                OutgoingAuthHeaders::HEADER_OUT_LOGIN,
                core::networking::Direction::Outgoing);

            packet->SetString("username", app->GetBotUserName());
            packet->SetString("password", app->GetBotPassword());

            connector->Send(packet);
        }
    }

    void AuthSession::HandleLoginSuccess(
        std::shared_ptr<core::networking::Packet> packet) {
        auto loginKey = packet->GetField<uint32_t>("key");
        auto result = packet->GetField<uint8_t>("result");

        if (!result) {
            CORE_LOGGING(trace)
                << "This should never happen (bad key)";
        }
        _application.lock()->SetLoginKey(loginKey);

        CORE_LOGGING(trace)
            << "Login key received: " << std::to_string(loginKey);
        _connector.lock()->Shutdown();
    }

    void AuthSession::HandleLoginFailed(
        std::shared_ptr<core::networking::Packet> packet) {
        auto result = packet->GetString("status");

        _application.lock()->SetLoginKey(0);
        CORE_LOGGING(trace) << "Login failed, error: " << result;
        _connector.lock()->Shutdown();
    }
}
