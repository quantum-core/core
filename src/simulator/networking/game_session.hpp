﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "session.hpp"

namespace simulator {
    namespace networking {
        class GameSession final
            : public SessionBase {
        public:
            GameSession(std::shared_ptr<Application> app,
                           std::shared_ptr<ClientConnector> connector);

            void RegisterHandlers() override;
            uint8_t GetType() override;

        protected:
            void OnPhaseChange(uint8_t oldPhase) override;

        private:
            // Login phase functions
            void RegisterLoginPhaseHandlers();
            void HandleLoginPhase(uint8_t oldPhase);
            void HandleEmpirePacketLogin(
                std::shared_ptr<core::networking::Packet> packet);

        private:
            // Select phase functions
            void RegisterSelectPhaseHandlers();
            void HandleSelectPhase(uint8_t oldPhase);
            void HandleCharactersPacketLogin(
                std::shared_ptr<core::networking::Packet> packet);

        private:
            // Loading phase functions
            void RegisterLoadingPhaseHandlers();
            void HandleLoadingPhase(uint8_t oldPhase);
            void HandleCharacterDetailsLoading(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleCharacterPointsLoading(
                std::shared_ptr<core::networking::Packet> packet);

        private:
            // Game phase functions
            void RegisterGamePhaseHandlers();
            void HandleGamePhase(uint8_t oldPhase);

            // Game phase handlers
            void HandleTimeSyncGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleChannelIDGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleAddCharacterGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleCharacterInfoGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleChatGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleCharacterMoveGame(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleRemoveCharacterGame(
                std::shared_ptr<core::networking::Packet> packet);

            // Game phase methods
            void SendChatMessage(const std::string& message,
                                 uint8_t chatMessageType);

        private:
            std::shared_ptr<GameSession> shared_from_this() {
             return std::static_pointer_cast<GameSession>(
                    SessionBase::shared_from_this());
            }
        };
    }
}
