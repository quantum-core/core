// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "../../core/logger.hpp"

#include <string>
#include <mutex>

#include <boost/asio.hpp>
#include <random>
#include <deque>

#include "../../core/networking/cryptation.hpp"
#include "../../core/networking/packet_manager.hpp"

namespace simulator {
    class Application;

    namespace networking {
        class SessionBase;
        class ClientConnector : public std::enable_shared_from_this<ClientConnector> {
        public:
            typedef std::function<void(
                std::shared_ptr<core::networking::Packet> packet)>
                PacketHandler;

            typedef std::function<void()> NewConnectionHandler;
            typedef std::function<void()> DisconnectHandler;

        public:
            ClientConnector(std::shared_ptr<Application> application,
                   boost::asio::io_context &context,
                   uint8_t securityLevel,
                   const uint32_t defaultKeys[]);
            virtual ~ClientConnector();

        public:
            void ConnectTo(const std::string &host, uint16_t port,
                           uint8_t connectType);
            void SetConnectInfo(const std::string &host, uint16_t port,
                                uint8_t connectType);
            void Connect();
            void Close();
            void Shutdown();

            std::weak_ptr<core::networking::PacketManager> GetPacketManager();

            void SetConnectionFailedHandler(NewConnectionHandler handler);
            void SetDisconnectHandler(DisconnectHandler handler);
            void RegisterHandler(uint8_t header, PacketHandler handler);

            void CallConnectionFailedHandler();
            void CallDisconnectHandler();
            void CallPacketHandler(std::shared_ptr<core::networking::Packet> packet);

            void SendMovementPacket();
            void Send(
                const std::shared_ptr<core::networking::Packet> &packet);

            std::shared_ptr<Application> GetApplication();
            std::shared_ptr<core::networking::CryptationBase> GetCryptation();
            uint8_t GetSecurityLevel();

        private:
            void Prepare() noexcept(false);

            void ReadNextPacket();
            void HandleConnect(const boost::system::error_code &error);
            void HandleReadHeader(const boost::system::error_code &err, std::size_t transferred);
            void HandleReadSize(std::shared_ptr<core::networking::Packet> packet,
                                const boost::system::error_code &err,
                                std::size_t transferred);
            void HandleReadData(std::shared_ptr<core::networking::Packet> packet,
                                const boost::system::error_code &err,
                                std::size_t transferred);
            void HandleWrite(const boost::system::error_code &err,
                             std::size_t transferred);
            void OnShutdown();
            void OnSend();

        private:
            std::weak_ptr<Application> _application;
            std::shared_ptr<core::networking::PacketManager> _packetManager;

            std::map<uint8_t, PacketHandler> _packetHandlers;
            NewConnectionHandler _connectionFailedHandler;
            DisconnectHandler _disconnectHandler;

            std::atomic<bool> _isShutdown;
            std::deque<std::shared_ptr<core::networking::Packet>>
                _sendQueue;
            std::mutex _sendMutex;

            uint8_t _securityLevel;
            uint32_t _defaultKeys[4];

            std::string _host;
            unsigned short _port;
            uint8_t _connectionType;

            std::mt19937 _random;

            boost::asio::streambuf _buffer;
            boost::asio::streambuf _writeBuffer;
            boost::asio::strand <
                boost::asio::io_context::executor_type> _strand;
            boost::asio::ip::tcp::socket _socket;

            std::shared_ptr<core::networking::CryptationBase> _cryptation;
            std::shared_ptr<SessionBase> _sessionManager;
        };
    } // namespace networking
} // namespace simulator
