﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "game_session.hpp"

#include "../../core/networking/cryptation.hpp"
#include "../application.hpp"
#include "game_headers.hpp"
#include "client_connector.hpp"
#include "reserved_headers.hpp"

namespace simulator::networking {
    GameSession::GameSession(std::shared_ptr<Application> app,
                             std::shared_ptr<ClientConnector> connector)
        : SessionBase(app, connector) {}

    void GameSession::RegisterHandlers() {
        SessionBase::RegisterHandlers();

        CORE_LOGGING(trace) << "Registering handlers for game session";
        auto connector = _connector.lock();

        RegisterLoginPhaseHandlers();
        RegisterSelectPhaseHandlers();
        RegisterLoadingPhaseHandlers();
        RegisterGamePhaseHandlers();
    }

    uint8_t GameSession::GetType() { return HandlerType::HANDLER_GAME; }

    void GameSession::OnPhaseChange(uint8_t oldPhase) {
        if (_phase == Phases::PHASE_LOGIN) {
            HandleLoginPhase(oldPhase);
        } else if (_phase == Phases::PHASE_SELECT) {
            HandleSelectPhase(oldPhase);
        } else if (_phase == Phases::PHASE_LOADING) {
            HandleLoadingPhase(oldPhase);
        } else if (_phase == Phases::PHASE_GAME) {
            HandleGamePhase(oldPhase);
        }
    }
}  // namespace simulator::networking
