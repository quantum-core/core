// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "client_connector.hpp"

#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <utility>

#include "auth_session.hpp"
#include "game_session.hpp"
#include "../application.hpp"
#include "reserved_headers.hpp"
#include "../../core/networking/packet.hpp"
#include "boost/format.hpp"

#include "../exit_codes.hpp"

namespace ip = boost::asio::ip;

namespace simulator::networking {
    ClientConnector::ClientConnector(std::shared_ptr<Application> application,
                   boost::asio::io_context &context,
                   uint8_t securityLevel,
                   const uint32_t defaultKeys[]) :
            _application(std::move(application)),
            _strand(boost::asio::make_strand(context.get_executor())),
            _socket(_strand),
            _port(0),
            _securityLevel(securityLevel),
            _isShutdown(true),
            _connectionType(HandlerType::HANDLER_NONE) {

        CORE_LOGGING(trace) << "Creating connector";
        memcpy(_defaultKeys, defaultKeys, sizeof(_defaultKeys));

        if (_securityLevel == SECURITY_LEVEL_KEY_AGREEMENT) {
            _cryptation =
                std::make_shared<core::networking::KeyAgreementCryptation>();
        } else if (_securityLevel == SECURITY_LEVEL_XTEA) {
            _cryptation = std::make_shared<core::networking::XTEACryptation>();
            _cryptation->AddData(core::networking::XTEA_CRYPTATION_START_KEY,
                                 _defaultKeys, 16);
        }
        _packetManager = std::make_shared<core::networking::PacketManager>();
    }

    ClientConnector::~ClientConnector() {
        CORE_LOGGING(trace) << "Destroying connector";
    }

    void ClientConnector::Prepare() {
        if (_cryptation) {
            _cryptation->Reset();
        }

        // Packets
        _packetManager->Clear();

        // Handlers
        _packetHandlers.clear();
        _connectionFailedHandler = nullptr;
        _disconnectHandler = nullptr;

        if (_connectionType == HandlerType::HANDLER_AUTH) {
            _sessionManager = std::static_pointer_cast<SessionBase>(
                std::make_shared<AuthSession>(_application.lock(),
                                              shared_from_this()));
        } else if (_connectionType == HandlerType::HANDLER_GAME) {
            _sessionManager = std::static_pointer_cast<SessionBase>(
                std::make_shared<GameSession>(_application.lock(),
                                              shared_from_this()));
        } else {
            CORE_LOGGING(error) << "Unknown connection type: " << std::to_string(_connectionType);
            std::exit(simulator::ErrorCodes::ERROR_UNKOWN_CONNECTION_TYPE);
        }
        _sessionManager->RegisterHandlers();
    }

    void ClientConnector::ConnectTo(const std::string &host, uint16_t port,
                                    uint8_t connectionType) {
        SetConnectInfo(host, port, connectionType);
        Connect();
    }

    void ClientConnector::SetConnectInfo(const std::string &host, uint16_t port,
                                         uint8_t connectionType) {
        _host = host;
        _port = port;
        _connectionType = connectionType;
    }

    void ClientConnector::Connect() {
        CORE_LOGGING(trace) << "Connecting to " << _host << ":" << _port;

        Prepare();

        using boost::asio::ip::tcp;
        tcp::resolver resolver(_application.lock()->GetContext());
        tcp::resolver::results_type endpoints = resolver.resolve(_host, std::to_string(_port));

        boost::asio::async_connect(_socket, endpoints, 
            boost::asio::bind_executor(_strand, boost::bind(&ClientConnector::HandleConnect,
            shared_from_this(),
            boost::asio::placeholders::error)));
    }

    void ClientConnector::Close() {
        _isShutdown = true;
        try {
            if (_socket.is_open()) {
                CORE_LOGGING(debug) << "Closing connection to " << _socket.remote_endpoint().address().to_string();
                _socket.close();
            }
        } catch (...) {
            CORE_LOGGING(error) << "Failed to close connection";
        }
        CallDisconnectHandler();
    }

    void ClientConnector::Shutdown() {
        if (_isShutdown) return;
        _isShutdown = true;
        boost::asio::post(
            _strand,
            boost::bind(&ClientConnector::OnShutdown, shared_from_this()));
    }

    void ClientConnector::OnShutdown() {
        // The healthiest way to gracefully terminate a connection is to make
        // the client disconnecting first using shutdown and only closing the
        // send part of the socket. This way the server can finalize the
        // procedure. Also this will force the client to call the disconnect
        // handler from the network's thread.
        _isShutdown = true;
        try {
            if (_socket.is_open()) {
                _socket.shutdown(boost::asio::ip::tcp::socket::shutdown_send);
                CORE_LOGGING(info) << "Shutting down connection";
            }
        } catch (const boost::system::system_error &) {
            CORE_LOGGING(warning) << "Failed to shutdown connection";
        }
    }

    void ClientConnector::HandleConnect(const boost::system::error_code &error) {
        if (!error) {
            CORE_LOGGING(trace) << "Successfully connected";
            _isShutdown = false;
            ReadNextPacket();
        } else {
            CORE_LOGGING(trace) << "Failed to connect, error: " << error.message();
            CallConnectionFailedHandler();
        }
    }

    void ClientConnector::ReadNextPacket() {
        CORE_LOGGING(trace) << "Reading next packet";
        boost::asio::async_read(
            _socket, _buffer.prepare(1), boost::asio::transfer_exactly(1),
            boost::asio::bind_executor(
                _strand,
                boost::bind(&ClientConnector::HandleReadHeader,
                            shared_from_this(),
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));
    }

    void ClientConnector::HandleReadHeader(const boost::system::error_code &err,
                                           std::size_t transferred) {
        _buffer.commit(transferred);
        if (!err) {
            CORE_LOGGING(trace) << "(HEADER) Received " << transferred << " bytes";

            uint8_t header;
            boost::asio::buffer_copy(boost::asio::buffer(&header, sizeof(header)), _buffer.data());

            if (_cryptation && _cryptation->IsReady()){
                _cryptation->Decrypt(&header, reinterpret_cast<const uint8_t *>(_buffer.data().data()), 1);
            }

            _buffer.consume(1);

            CORE_LOGGING(trace) << "Received header " << boost::format("0x%02x") % (int)header;

            auto packet = _packetManager->CreatePacket(header, core::networking::Incoming);
            if (!packet) {
                CORE_LOGGING(error) << "Received unknown header " << boost::format("0x%02x") % (int)header;
                std::exit(simulator::ErrorCodes::ERROR_UNKNOWN_PACKET_HEADER);
            }

            // Check if packet is dynamic size
            if (packet->IsDynamicSized()) {
                CORE_LOGGING(trace) << "Reading dynamic packet size";

                // We have to read our packet size first
                boost::asio::async_read(
                    _socket, _buffer.prepare(2),
                    boost::asio::transfer_exactly(2),
                    boost::asio::bind_executor(
                        _strand,
                        boost::bind(
                            &ClientConnector::HandleReadSize,
                            shared_from_this(), packet,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));

                return;
            }

            auto sizeToRead = packet->GetSize();
            CORE_LOGGING(trace) << "Reading the next " << sizeToRead << " bytes";

            boost::asio::async_read(
                _socket, _buffer.prepare(sizeToRead),
                boost::asio::transfer_exactly(sizeToRead),
                boost::asio::bind_executor(
                    _strand,
                    boost::bind(&ClientConnector::HandleReadData,
                                shared_from_this(), packet,
                                boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred)));
        }
        else
        {
            if (err == boost::asio::error::eof)
            {
                CORE_LOGGING(debug) << "Connection closed by remote";
                Close();
                return;
            }
            CORE_LOGGING(warning) << "Failed to read " << err.message();
            Close();
        }
    }

    void ClientConnector::HandleReadSize(
        std::shared_ptr<core::networking::Packet> packet,
        const boost::system::error_code &err, std::size_t transferred) {
        CORE_LOGGING(trace) << "(SIZE) Received " << transferred << " bytes";
        _buffer.commit(transferred);
        if (!err) {
            assert(transferred == 2);
            unsigned short size;

            if (_cryptation && _cryptation->IsReady()) {
                std::vector<uint8_t> buf(transferred);

                _cryptation->Decrypt(
                    buf.data(),
                    reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                    transferred);
                _buffer.consume(transferred);

                size = (buf[1] << 8) | buf[0];
            } else {
                boost::asio::buffer_copy(
                    boost::asio::buffer(static_cast<void *>(&size), 2),
                    _buffer.data());  // todo: clean solution
            }

            CORE_LOGGING(trace) << "Size of packet is " << size;

            auto sizeToRead =
                size - 1 - 2;  // already read header (1byte) and size (2bytes)

            CORE_LOGGING(trace)
                << "Reading the next " << sizeToRead << " bytes";
            boost::asio::async_read(
                _socket, _buffer.prepare(sizeToRead),
                boost::asio::transfer_exactly(sizeToRead),
                boost::asio::bind_executor(
                    _strand,
                    boost::bind(&ClientConnector::HandleReadData,
                                shared_from_this(), packet,
                                boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred)));
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug) << "Connection closed by remote";
                Close();
                return;
            }
            CORE_LOGGING(warning) << "Failed to read " << err.message();
            Close();
        }
    }

    void ClientConnector::HandleReadData(std::shared_ptr<core::networking::Packet> packet,
                                    const boost::system::error_code& err,
                                    std::size_t transferred) {
        CORE_LOGGING(trace) << "(DATA) Received " << transferred << " bytes";
        _buffer.commit(transferred);

        if (!err) {
            if (_cryptation && _cryptation->IsReady()) {
                std::vector<uint8_t> buf(transferred);

                _cryptation->Decrypt(
                    buf.data(),
                    reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                    transferred);
                _buffer.consume(transferred);

                packet->CopyData(buf, packet->IsDynamicSized() ? 2 : 0);
            } else {
                packet->CopyData(_buffer);
            }

#ifdef NDEBUG
            try {
#endif
                {
                    // PROFILE_SCOPE("Handler for " + packet->GetName());
                    CallPacketHandler(packet);
                }
#ifdef NDEBUG
            } catch (...) {
                CORE_LOGGING(fatal)
                    << boost::current_exception_diagnostic_information();
            }
#endif

            ReadNextPacket();
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug) << "Connection closed by remote";
                Close();
                return;
            }
            CORE_LOGGING(warning) << "Failed to read " << err.message();
            Close();
        }
    }

    std::shared_ptr<core::networking::CryptationBase>
    ClientConnector::GetCryptation() {
        return _cryptation;
    }

    uint8_t ClientConnector::GetSecurityLevel() { return _securityLevel; }

    std::weak_ptr<core::networking::PacketManager> ClientConnector::GetPacketManager() {
        return _packetManager;
    }

    void ClientConnector::SetConnectionFailedHandler(NewConnectionHandler handler) {
        _connectionFailedHandler = handler;
    }

    void ClientConnector::SetDisconnectHandler(DisconnectHandler handler) {
        _disconnectHandler = handler;
    }

    void ClientConnector::RegisterHandler(uint8_t header, PacketHandler handler) {
        CORE_LOGGING(trace) << "Register handler for " << std::hex << (int) header;
        _packetHandlers[header] = handler;
    }

    void ClientConnector::CallConnectionFailedHandler() {
        if (_connectionFailedHandler) {
            _connectionFailedHandler();
        }
    }

    void ClientConnector::CallDisconnectHandler() {
        if (_disconnectHandler) {
            _disconnectHandler();
        }
    }

    void ClientConnector::CallPacketHandler(std::shared_ptr<core::networking::Packet> packet) {
        auto it = _packetHandlers.find(packet->GetHeader());
        if (it != _packetHandlers.end()) {
            it->second(packet);
            return;
        }
    }

    std::shared_ptr<Application> ClientConnector::GetApplication() {
        return _application.lock();
    }

    void ClientConnector::SendMovementPacket() {
    
    }

    void ClientConnector::Send(
        const std::shared_ptr<core::networking::Packet> &packet) {
        if (_isShutdown) return;
        std::lock_guard<std::mutex> guard(_sendMutex);
        _sendQueue.push_back(packet);
        if (_sendQueue.size() == 1) {
            boost::asio::post(_strand, boost::bind(&ClientConnector::OnSend,
                                                   shared_from_this()));
        }
    }

    void ClientConnector::OnSend() {
        if (_isShutdown || !_socket.is_open()) return;
        std::shared_ptr<core::networking::Packet> packet = nullptr;
        {
            std::lock_guard<std::mutex> guard(_sendMutex);
            if (_sendQueue.empty()) return;
            packet = _sendQueue.front();
        }
        CORE_LOGGING(trace) << "Sending packet " << packet->GetName() << " ("
                            << (uint32_t)packet->GetHeader() << ")";

        auto data = packet->GetData();
        uint8_t header = packet->GetHeader();
        size_t bonusSize = 0;
        if (packet->HasSequence()) {
            bonusSize += 1;
        }

        auto buffer = _writeBuffer.prepare(1 + data.size() + bonusSize);

        if (_cryptation && _cryptation->IsReady()) {
            _cryptation->Encrypt(static_cast<uint8_t*>(buffer.data()), &header, 1);
            if (data.size()) {
                _cryptation->Encrypt(static_cast<uint8_t *>(buffer.data()) + 1,
                                     data.data(), data.size());
            }
            if (packet->HasSequence()) {
                _cryptation->Encrypt(
                    static_cast<uint8_t *>(buffer.data()) + 1 + data.size(),
                    &header, sizeof(header));
            }
        } else {
            boost::asio::buffer_copy(boost::asio::buffer(buffer, 1),
                                     boost::asio::buffer(&header, 1));
            boost::asio::buffer_copy(boost::asio::buffer(buffer + 1, data.size()),
                                     boost::asio::buffer(data.data(), data.size()));
            if (packet->HasSequence()) {
                // whatever, sequence is not implemented right now, just leave
                // uninitialized
            }
        }
        _writeBuffer.commit(buffer.size());
        _socket.async_write_some(
            buffer,
            boost::asio::bind_executor(
                _strand,
                boost::bind(&ClientConnector::HandleWrite, shared_from_this(),
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));
    }

    void ClientConnector::HandleWrite(const boost::system::error_code &err,
                                 std::size_t transferred) {
        _writeBuffer.consume(transferred);
        if (err) {
            CORE_LOGGING(warning) << "Failed to send data: " << err.message();
        } else {
            {
                std::lock_guard<std::mutex> guard(_sendMutex); 
                if (_sendQueue.empty()) return;
                _sendQueue.pop_front();
                if (_sendQueue.empty()) return;
            }
            OnSend();
        }
    }
}
