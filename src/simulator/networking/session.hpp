﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "../../core/logger.hpp"
#include "../../core/networking/packet.hpp"

namespace simulator {
    class Application;

    namespace networking {
        class ClientConnector;

        enum HandlerType : uint8_t {
            HANDLER_NONE = 0,
            HANDLER_AUTH = 1,
            HANDLER_GAME = 2
        };

        class SessionBase : public std::enable_shared_from_this<SessionBase> {
        public:
            SessionBase(std::shared_ptr<Application> app,
                           std::shared_ptr<ClientConnector> connector);
            virtual ~SessionBase();

            virtual void RegisterHandlers();

            [[nodiscard]] virtual uint8_t GetType();

        private:
            void HandleHandshake(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleKeyAgreement(
                std::shared_ptr<core::networking::Packet> packet);
            void HandlePhase(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleKeyAgreementCompleted(
                std::shared_ptr<core::networking::Packet> packet);

            void SendHandshake(uint32_t time, uint32_t delta);

        protected:
            virtual void OnPhaseChange(uint8_t oldPhase) = 0;
            void RegisterDefaultPackets(bool onlyPhase = false);

        protected:
            bool _handshaking;
            uint32_t _handshake;
            uint8_t _phase;

            std::weak_ptr<Application> _application;
            std::weak_ptr<ClientConnector> _connector;
        };
    }
}
