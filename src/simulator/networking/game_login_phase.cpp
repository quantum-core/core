// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "game_session.hpp"
#include "game_headers.hpp"
#include "packets/game_login_packets.hpp"
#include "client_connector.hpp"
#include "../application.hpp"

namespace simulator {
    namespace networking {
        void GameSession::RegisterLoginPhaseHandlers() {
            auto connector = _connector.lock();
            connector->RegisterHandler(
                IncomingGameLoginHeaders::HEADER_INC_EMPIRE,
                std::bind(&GameSession::HandleEmpirePacketLogin, shared_from_this(),
                          std::placeholders::_1));
        }

        void GameSession::HandleEmpirePacketLogin(
            std::shared_ptr<core::networking::Packet> packet) {
            CORE_LOGGING(trace) << "Receive empire packet";
            // whatever
        }

        void GameSession::HandleLoginPhase(uint8_t oldPhase) {
            auto connector = _connector.lock();
            auto app = _application.lock();
            auto packetManager = connector->GetPacketManager().lock();
            packetManager->Clear();
            RegisterDefaultPackets(true);
            packets::RegisterGame_login_packets(packetManager);

            CORE_LOGGING(trace) << "Send login packet";
            auto packet = packetManager->CreatePacket(
                OutgoingGameLoginHeaders::HEADER_OUT_LOGIN,
                core::networking::Direction::Outgoing);

            packet->SetString("username", app->GetBotUserName());
            packet->SetField<uint32_t>("key", app->GetLoginKey());

            connector->Send(packet);
        }
    }
}  // namespace simulator