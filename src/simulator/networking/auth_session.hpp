﻿// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "session.hpp"

namespace simulator {
    namespace networking {
        class AuthSession final : public SessionBase {
        public:
            AuthSession(std::shared_ptr<Application> app,
                           std::shared_ptr<ClientConnector> connector);

            void RegisterHandlers() override;
            uint8_t GetType() override;

        protected:
            void OnPhaseChange(uint8_t oldPhase) override;

        private:
            void HandleLoginSuccess(
                std::shared_ptr<core::networking::Packet> packet);
            void HandleLoginFailed(
                std::shared_ptr<core::networking::Packet> packet);

        private:
            std::shared_ptr<AuthSession> shared_from_this() {
             return std::static_pointer_cast<AuthSession>(
                    SessionBase::shared_from_this());
            }
        };
    }
}
