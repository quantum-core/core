// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/program_options.hpp>

namespace simulator {
    namespace networking {
        class ClientConnector;
    }
    namespace entity {
        class EntityManager;
    }
    namespace simulations {
        class Simulation;
    }
    class Application : public std::enable_shared_from_this<Application> {
    public:
        struct BotOptions {
            std::string userName, password;
            uint8_t defaultChar;

            std::string authIP, gameIP;
            uint16_t authPort, gamePort;
            bool tryReconnect, reconnectIfFailed;
        };
    public:
        Application();
        ~Application();

        [[nodiscard]] static std::shared_ptr<Application> GetInstance();
        [[nodiscard]] boost::asio::io_context& GetContext();
        [[nodiscard]] uint32_t GetCoreTime();
        [[nodiscard]] uint32_t GetServerTime();
        [[nodiscard]] std::shared_ptr<entity::EntityManager>
        GetEntityManager() {
            return _entityManager;
        }

        [[nodiscard]] std::shared_ptr<networking::ClientConnector>
        GetConnector();
        [[nodiscard]] std::shared_ptr<simulations::Simulation> GetSimulation() {
            return _simulation;
        }

        [[nodiscard]] uint32_t GetLoginKey() { return _loginKey; }
        [[nodiscard]] const std::string& GetBotUserName() {
            return _botOptions.userName;
        }
        [[nodiscard]] const std::string& GetBotPassword() {
            return _botOptions.password;
        }

        void SetLoginKey(uint32_t key) { _loginKey = key; }
        int SetOptions(boost::program_options::variables_map& vm,
                       std::vector<std::string>& unrecognizedOptions);
        void SetServerTime(uint32_t tm) { _serverTime = tm; }

        void Start();

    private:
        void Update(uint32_t elapsedTime);
        void PrepareHandlers();
        void OnDisconnect();
        void OnFailedToConnect();

    private:
        std::thread _networkThread;
        boost::asio::io_context _context;
        boost::posix_time::ptime _bootTime;
        std::atomic<uint32_t> _serverTime;
        BotOptions _botOptions;

        std::shared_ptr<networking::ClientConnector> _connector;
        std::shared_ptr<entity::EntityManager> _entityManager;
        std::shared_ptr<simulations::Simulation> _simulation;

        uint32_t _loginKey;
        static std::shared_ptr<Application> _instance;
    };
} // namespace simulator
