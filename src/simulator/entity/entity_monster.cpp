// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "entity_monster.hpp"

#include "entity.hpp"

namespace simulator::entity {
    EntityMonster::EntityMonster(uint32_t vid, int32_t x, int32_t y, float angle, uint16_t objClass, uint8_t state,
                                 uint8_t movementSpeed, uint8_t attackSpeed)
        : Entity::Entity(vid, x, y, angle, objClass, state, movementSpeed, attackSpeed) {}
}  // namespace simulator::entity
