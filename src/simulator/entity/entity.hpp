// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include "../../core/logger.hpp"
#include <mutex>

namespace simulator {
    namespace entity {
        class EntityManager;
        class Entity {
        public:
            enum EntityType : uint8_t {
                ENTITY_MONSTER = 0,
                ENTITY_NPC = 1,
                ENTITY_PLAYER = 6,

                ENTITY_MAX_NUM,
                ENTITY_NONE = ENTITY_MAX_NUM
            };
            enum MovementTypes : uint8_t {
                WAIT = 0,
                MOVE = 1,
                ATTACK = 2,
                COMBO = 3,
                MOB_SKILL = 4,
                MAX,
                SKILL = 0x80  // why?!
            };

        public:
            Entity(uint32_t vid, int32_t x, int32_t y, float angle,
                   uint16_t objClass, uint8_t state, uint8_t movementSpeed,
                   uint8_t attackSpeed);
            virtual ~Entity() = default;

            [[nodiscard]] int32_t GetX();
            [[nodiscard]] int32_t GetY();

            void MoveTo(int32_t x, int32_t y);
            void StopMove();
            void SetMain(bool isMain);

            bool UpdateMovement(uint32_t elapsedTime);
            virtual uint8_t GetType() const { return EntityType::ENTITY_NONE; }
            virtual std::shared_ptr<Entity> GetPointer() = 0;

        protected:
            static void SendMovementPacket(int32_t x, int32_t y, uint8_t type,
                                    uint8_t arg, uint8_t rot);

        protected:
            std::mutex _entityMutex;

            uint32_t _vid;
            int32_t _x, _y;
            float _rotation;
            uint16_t _class;
            uint8_t _state, _movementSpeed, _attackSpeed;

            int32_t _goingX, _goingY;
            int32_t _startX, _startY;
            uint32_t _movementAccumulator, _movementDuration;
            bool _isMain, _isMoveUpdating;

        private:
        };
    }
}