// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "entity.hpp"
#include "entity_manager.hpp"
#include "../application.hpp"
#include "../../core/utils/math.hpp"
#include "../networking/client_connector.hpp"
#include "../networking/game_headers.hpp"
#include "../../core/networking/packet.hpp"
#include "../simulations/simulation.hpp"

namespace simulator {
    namespace entity {
        Entity::Entity(uint32_t vid, int32_t x, int32_t y, float angle,
                       uint16_t objClass,
                       uint8_t state, uint8_t movementSpeed,
                       uint8_t attackSpeed)
            : _vid(vid),
              _x(x),
              _y(y),
              _rotation(angle),
              _goingX(0),
              _goingY(0),
              _startX(0),
              _startY(0),
              _class(objClass),
              _state(state),
              _movementSpeed(movementSpeed),
              _attackSpeed(attackSpeed),
              _movementDuration(0),
              _movementAccumulator(0),
              _isMoveUpdating(false),
              _isMain(false) {}

        int32_t Entity::GetX() {
            std::lock_guard<std::mutex> guard(_entityMutex);
            return _x;
        }

        int32_t Entity::GetY() {
            std::lock_guard<std::mutex> guard(_entityMutex);
            return _y;
        }

        void Entity::MoveTo(int32_t x, int32_t y) {
            std::lock_guard<std::mutex> guard(_entityMutex);
            if (_goingX == x && _goingY == y) return;
            CORE_LOGGING(trace) << "Moving to " << x << ", " << y << " from "
                                << _x << " " << _y;
            _startX = _x;
            _startY = _y;
            _goingX = x;
            _goingY = y;
            _movementAccumulator = 0;
            _rotation = core::utils::Math::CalculateRotation(
                static_cast<float>(_goingX), static_cast<float>(_goingY));

            float distance = core::utils::Math::Distance(_startX, _startY, _goingX, _goingY);
            float animationSpeed = 300.0f / 0.666667f;
            //float animationSpeed =
            //-animation->accumulationY / animation->motionDuration;

            int i = 100 - _movementSpeed;
            if (i > 0) {
                i = 100 + i;
            } else if (i < 0) {
                i = 10000 / (100 - i);
            } else {
                i = 100;
            }
            _movementDuration = static_cast<uint32_t>(
                static_cast<int>((distance / animationSpeed) * 1000) * i / 100);

            _state = MovementTypes::MOVE;
            if (!_isMoveUpdating) {
                _isMoveUpdating = true;
                Application::GetInstance()->GetEntityManager()->MoveEntity(
                    GetPointer());
            }
            if (_isMain) {
                SendMovementPacket(x, y, _state, 0, _rotation);
            }
        }

        void Entity::StopMove() {
            std::lock_guard<std::mutex> guard(_entityMutex);
            _isMoveUpdating = false;
            _state = MovementTypes::WAIT;
            if (_isMain) {
                SendMovementPacket(_x, _y, _state, 0, _rotation);
            }
        }

        void Entity::SetMain(bool isMain) {
            std::lock_guard<std::mutex> guard(_entityMutex);
            _isMain = isMain;
        }

        bool Entity::UpdateMovement(uint32_t elapsedTime) { 
            bool moveDone = false;
            bool isMain = false;
            int32_t startX, startY, vid;
            {
                std::lock_guard<std::mutex> guard(_entityMutex);

                if (_state != MovementTypes::MOVE) {
                    return true;
                }

                _movementAccumulator += elapsedTime;

                auto rate = _movementAccumulator / (float)_movementDuration;
                if (rate > 1.0f) rate = 1.0f;

                _x = (int32_t)((float)(_goingX - _startX) * rate + _startX);
                _y = (int32_t)((float)(_goingY - _startY) * rate + _startY);

                if (rate >= 1.0f) {
                    moveDone = true;
                    startX = _startX;
                    startY = _startY;
                }
                isMain = _isMain;
                vid = _vid;
            }

            // todo: add callback when movement completed
            if (moveDone) {
                return Application::GetInstance()
                    ->GetSimulation()
                    ->MovementComplete(isMain, vid, startX, startY);
            }
            return false;
        }

        void Entity::SendMovementPacket(int32_t x, int32_t y, uint8_t type,
                                        uint8_t arg, uint8_t rot) {
            auto packet =
                Application::GetInstance()
                    ->GetConnector()
                    ->GetPacketManager()
                    .lock()
                    ->CreatePacket(OutgoingGameHeaders::HEADER_OUT_MOVE);
            packet->SetField<uint8_t>("movementType", type);
            packet->SetField<uint8_t>("argument", arg);
            packet->SetField<uint8_t>(
                "rotation",
                static_cast<uint8_t>(static_cast<float>(rot) / 5.0f));
            packet->SetField<int32_t>("x", x);
            packet->SetField<int32_t>("y", y);
            packet->SetField<int32_t>(
                "time", Application::GetInstance()->GetServerTime());
            Application::GetInstance()->GetConnector()->Send(packet);
        }
    }  // namespace entity
}  // namespace simulator
