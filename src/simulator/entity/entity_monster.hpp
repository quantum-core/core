// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include "entity.hpp"

namespace simulator::entity {
    class EntityMonster final : public Entity, public std::enable_shared_from_this<EntityMonster> {
       public:
        EntityMonster(uint32_t vid, int32_t x, int32_t y, float angle, uint16_t objClass, uint8_t state,
                      uint8_t movementSpeed, uint8_t attackSpeed);
        ~EntityMonster() override = default;

        uint8_t GetType() const override { return EntityType::ENTITY_MONSTER; }
        std::shared_ptr<Entity> GetPointer() override { return shared_from_this(); }
    };
}  // namespace simulator::entity