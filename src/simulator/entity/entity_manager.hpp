// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include "entity.hpp"
#include "entity_player.hpp"
#include <unordered_map>
#include <list>
namespace simulator {
    namespace entity {
        class EntityManager final {
        public:
            EntityManager();
            ~EntityManager();

            void Clear();

            void CreateEntity(uint32_t vid, uint8_t entityType, int32_t x,
                              int32_t y, float angle, uint16_t objClass,
                              uint8_t state, uint8_t movementSpeed,
                              uint8_t attackSpeed);
            void RemoveEntity(uint32_t vid);
            void MoveEntity(std::shared_ptr<Entity> ent);
            void Update(uint32_t elapsedTime);

            void SetMainCharacter(uint32_t vid);
            std::shared_ptr<EntityPlayer> GetMainCharacter();

            std::shared_ptr<Entity> GetEntity(uint32_t vid,
                                              bool shouldExists = true);
            std::shared_ptr<EntityPlayer> GetPlayer(uint32_t vid,
                                              bool shouldBePlayer = true);

        private:
            std::mutex _managerMutex, _updateMutex;
            std::shared_ptr<EntityPlayer> _mainCharacter;
            std::atomic<bool> _isMainSet;

            std::unordered_map<uint32_t, std::shared_ptr<Entity>> _entityMap;
            std::list<std::shared_ptr<Entity>> _updateList;
        };
    }
}