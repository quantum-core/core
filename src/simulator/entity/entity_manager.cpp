// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "entity_manager.hpp"

#include "../application.hpp"
#include "../exit_codes.hpp"
#include "../simulations/simulation.hpp"
#include "entity_monster.hpp"
#include "entity_player.hpp"

namespace simulator {
    namespace entity {
        EntityManager::EntityManager() : _isMainSet(false) {
            CORE_LOGGING(trace) << "Creating entity manager";
        }
        EntityManager::~EntityManager() {
            CORE_LOGGING(trace) << "Destroying entity manager";
        }

        void EntityManager::Clear() {
            CORE_LOGGING(trace) << "Clear entity manager";
            std::lock_guard<std::mutex> guard(_managerMutex);
            std::lock_guard<std::mutex> guard2(_updateMutex);
            _entityMap.clear();
            _updateList.clear();
            _mainCharacter = nullptr;
            _isMainSet = false;
        }

        void EntityManager::CreateEntity(uint32_t vid,
                                         uint8_t entityType, int32_t x,
                                         int32_t y, float angle,
                                         uint16_t objClass, uint8_t state,
                                         uint8_t movementSpeed,
                                         uint8_t attackSpeed) {
            CORE_LOGGING(trace)
                << "Creating entity with vid " << std::to_string(vid);
            std::lock_guard<std::mutex> guard(_managerMutex);
            if (entityType == Entity::EntityType::ENTITY_PLAYER) {
                auto result = _entityMap.insert(std::make_pair(
                    vid,
                    std::make_shared<EntityPlayer>(vid, x, y, angle, objClass, state, movementSpeed, attackSpeed)));
                if (!result.second) {
                    CORE_LOGGING(error) << "Entity with vid " << std::to_string(vid) << " does already exists";
                    std::exit(simulator::ErrorCodes::ERROR_ENTITY_EXISTS);
                }
            } else if (entityType == Entity::EntityType::ENTITY_MONSTER) {
                auto result = _entityMap.insert(std::make_pair(
                    vid,
                    std::make_shared<EntityMonster>(vid, x, y, angle, objClass, state, movementSpeed, attackSpeed)));
                if (!result.second) {
                    CORE_LOGGING(error) << "Entity with vid " << std::to_string(vid) << " does already exists";
                    std::exit(simulator::ErrorCodes::ERROR_ENTITY_EXISTS);
                }
            } else {
                CORE_LOGGING(error)
                    << "Unknown entity type " << std::to_string(entityType);
                std::exit(simulator::ErrorCodes::ERROR_UNKNOWN_ENTITY_TYPE);
            }
        }

        void EntityManager::RemoveEntity(uint32_t vid) {
            CORE_LOGGING(trace)
                << "Removing entity with vid " << std::to_string(vid);

            std::lock_guard<std::mutex> guard(_managerMutex);
            auto it = _entityMap.find(vid);
            if (it == _entityMap.end()) {
                CORE_LOGGING(error)
                    << "Entity doesn't exists " << std::to_string(vid);
                std::exit(
                    simulator::ErrorCodes::ERROR_ENTITY_NOT_EXISTS_ON_REMOVE);
            } else {
                _entityMap.erase(it);
            }
        }

        void EntityManager::MoveEntity(std::shared_ptr<Entity> ent) {
            std::lock_guard<std::mutex> guard(_updateMutex);
            _updateList.push_back(ent);
        }

        void EntityManager::Update(uint32_t elapsedTime) {
            std::lock_guard<std::mutex> guard(_updateMutex);
            for (auto it = _updateList.begin(); it != _updateList.end();) {
                if ((*it)->UpdateMovement(elapsedTime)) {
                    (*it)->StopMove();
                    it = _updateList.erase(it);
                } else {
                    ++it;
                }
            }
        }

        void EntityManager::SetMainCharacter(uint32_t vid) {
            if (_isMainSet.exchange(true)) return;
            CORE_LOGGING(trace)
                << "Setting main character vid to " << std::to_string(vid);
            {
                auto player = GetPlayer(vid, false);
                std::lock_guard<std::mutex> guard(_managerMutex);
                _mainCharacter = std::move(player);
                if (!_mainCharacter) {
                    CORE_LOGGING(error) << "Couldn't set main character "
                                           "because it's vid doesn't exists: "
                                        << std::to_string(vid);
                    std::exit(simulator::ErrorCodes::
                                  ERROR_ENTITY_MAIN_CHAR_NOT_EXISTS);
                }
                _mainCharacter->SetMain(true);
            }
            Application::GetInstance()->GetSimulation()->OnMainInstanceLoad();
        }

        std::shared_ptr<EntityPlayer> EntityManager::GetMainCharacter() {
            std::lock_guard<std::mutex> guard(_managerMutex);
            return _mainCharacter;
        }

        std::shared_ptr<Entity> EntityManager::GetEntity(uint32_t vid,
                                                         bool shouldExists) {
            CORE_LOGGING(trace)
                << "Getting entity with vid " << std::to_string(vid);

            std::lock_guard<std::mutex> guard(_managerMutex);
            auto it = _entityMap.find(vid);
            if (it == _entityMap.end()) {
                if (shouldExists) {
                    CORE_LOGGING(error)
                        << "Entity doesn't exists " << std::to_string(vid);
                    std::exit(
                        simulator::ErrorCodes::ERROR_ENTITY_NOT_EXISTS_ON_GET);
                }
                return nullptr;
            }

            return it->second;
        }

        std::shared_ptr<EntityPlayer> EntityManager::GetPlayer(
            uint32_t vid, bool shouldBePlayer) {
            auto it = GetEntity(vid, shouldBePlayer);
            if (!it || it->GetType() != Entity::EntityType::ENTITY_PLAYER) {
                if (shouldBePlayer) {
                    CORE_LOGGING(error) << "Entity " << std::to_string(vid)
                                        << " is not a player";
                    std::exit(simulator::ErrorCodes::ERROR_ENTITY_NOT_PLAYER);
                }
                return nullptr;
            }

            return std::static_pointer_cast<EntityPlayer>(it);
        }
    } // namespace entity
} // namespace simulator