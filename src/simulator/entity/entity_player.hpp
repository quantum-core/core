// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include "entity.hpp"

namespace simulator {
    namespace entity {
        class EntityPlayer final : public Entity, public std::enable_shared_from_this<EntityPlayer> {
        public:
            EntityPlayer(uint32_t vid, int32_t x, int32_t y, float angle, uint16_t objClass,
                         uint8_t state, uint8_t movementSpeed,
                         uint8_t attackSpeed);
            ~EntityPlayer() override = default;

            uint8_t GetType() const override {
                return EntityType::ENTITY_PLAYER;
            }
            std::shared_ptr<Entity> GetPointer() override {
                return shared_from_this();
            }

            void GetName(std::string& name);
            void SetName(const std::string& name);

        private:
            std::string _name;
        };
    }
}