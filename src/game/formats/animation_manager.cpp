// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "animation_manager.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"
#include "../application.hpp"
#include "structured_file_reader.hpp"

namespace fs = boost::filesystem;

namespace game::formats {
    AnimationManager::AnimationManager() : _animations() {}

    AnimationManager::~AnimationManager() {
        for (auto entry : _animations) {
            for (auto animation : entry.second) {
                delete animation.second;
            }
        }
    }

    void AnimationManager::Load() {
        PROFILE_FUNCTION();
        fs::path data{"data/"};

        if (!fs::exists(data)) {
            CORE_LOGGING(error) << "Failed to load " << data.filename();
            return;
        }

        for (const auto &directory : fs::directory_iterator(data)) {
            auto path = directory.path();
            if (!fs::is_directory(path)) continue;

            // Depending on which folder we are processing we have to
            // "sort" the animations differently
            // pc and pc2 are player animations
            if (path.filename() == "pc") {
                LoadPlayerAnimations(path, false);
            }
            if (path.filename() == "pc2") {
                LoadPlayerAnimations(path, true);
            }
        }

        auto game = Application::GetInstance();
        auto &monsters = game->GetMonsters();
        for (auto monster : monsters) {
            fs::path monsterFolder = data / "monster" / monster.second.folder;
            fs::path monster2Folder = data / "monster2" / monster.second.folder;

            if (fs::exists(monsterFolder)) {
                LoadMonsterAnimations(monster.first, monsterFolder);
            } else if (fs::exists(monster2Folder)) {
                LoadMonsterAnimations(monster.first, monster2Folder);
            } else {
                CORE_LOGGING(warning)
                    << "Missing monster folder for " << monster.second.id;
            }
        }
    }

    void AnimationManager::LoadPlayerAnimations(const fs::path &dir,
                                                bool isPC2) {
        for (const auto &directory : fs::directory_iterator(dir)) {
            auto path = directory.path();
            if (!fs::is_directory(path)) continue;

            // Convert folder name to id
            uint32_t id = 0;
            auto dirname = path.filename();
            if (dirname == "warrior") {
                id = 0;
            } else if (dirname == "assassin") {
                id = 1;
            } else if (dirname == "sura") {
                id = 2;
            } else if (dirname == "shaman") {
                id = 3;
            }
            if (isPC2) {
                id += 4;
            }

            auto walkPath = fs::path(path).append("general").append("walk.msa");
            auto runPath = fs::path(path).append("general").append("run.msa");

            PutAnimation(id, AnimationType::WALK, AnimationSubType::GENERAL,
                         LoadAnimation(walkPath));
            PutAnimation(id, AnimationType::RUN, AnimationSubType::GENERAL,
                         LoadAnimation(runPath));
        }
    }

    void AnimationManager::LoadMonsterAnimations(
        uint32_t id, const boost::filesystem::path &dir) {
        auto motlist = dir / "motlist.txt";
        std::ifstream stream(motlist.string());

        std::string line;
        while (std::getline(stream, line)) {
            std::vector<std::string> res;
            boost::split(res, line, boost::is_any_of(" \t"),
                         boost::token_compress_on);

            if (res.size() != 4) continue;
            if (res[0] != "GENERAL") continue;

            if (res[1] == "RUN") {
                auto animation = LoadAnimation(dir / res[2]);
                PutAnimation(id, AnimationType::RUN, AnimationSubType::GENERAL,
                             animation);
            } else if (res[1] == "WALK") {
                auto animation = LoadAnimation(dir / res[2]);
                PutAnimation(id, AnimationType::WALK, AnimationSubType::GENERAL,
                             animation);
            }
        }
    }

    Animation *AnimationManager::LoadAnimation(
        const boost::filesystem::path &msa) {
        if (!fs::exists(msa)) {
            CORE_LOGGING(error) << "File " << msa << " doesn't exists";
            return nullptr;
        }

        CORE_LOGGING(trace) << "Load " << msa;

        auto file = StructuredFileReader::Read(msa.string());
        if (file.GetValue("ScriptType") != "MotionData") {
            CORE_LOGGING(error)
                << "Failed to load " << msa << " invalid ScriptType";
            return nullptr;
        }

        float duration, x = 0, y = 0, z = 0;

        if (!file.Contains("MotionDuration")) {
            CORE_LOGGING(warning)
                << "No motion duration in animation file " << msa;
            return nullptr;
        } else {
            duration = file.GetValue<float>("MotionDuration");
        }
        if (!file.Contains("Accumulation")) {
            CORE_LOGGING(warning)
                << "No accumulation in animation file " << msa;
        } else {
            x = file.GetValue<float>("Accumulation", 0);
            y = file.GetValue<float>("Accumulation", 1);
            z = file.GetValue<float>("Accumulation", 2);
        }

        auto ret = new Animation();
        ret->motionDuration = duration;
        ret->accumulationX = x;
        ret->accumulationY = y;
        ret->accumulationZ = z;
        return ret;
    }

    void AnimationManager::PutAnimation(uint32_t id, AnimationType type,
                                        AnimationSubType subType,
                                        Animation *animation) {
        uint32_t animationId = type | (subType << 24);

        CORE_LOGGING(trace) << "Add animation " << animationId << " for " << id;
        if (_animations.count(id) == 0) {
            _animations[id] = std::map<uint32_t, Animation *>();
        }
        _animations[id][animationId] = animation;
    }

    const Animation *AnimationManager::GetAnimation(
        uint32_t id, AnimationType type, AnimationSubType subType) const {
        if (_animations.count(id) == 0) return nullptr;
        uint32_t animationId = type | (subType << 24);

        auto animations = _animations.at(id);
        if (animations.count(animationId) == 0) return nullptr;

        return animations.at(animationId);
    }
}  // namespace game::formats