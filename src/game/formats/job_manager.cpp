// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "job_manager.hpp"

#include <cpptoml.h>

#include "../../core/logger.hpp"

namespace game::formats {
    JobManager::JobManager() = default;

    JobManager::~JobManager() = default;

    void JobManager::Load() {
        try {
            auto file = cpptoml::parse_file("data/jobs.toml");

            auto jobArray = file->get_table_array("job");
            for (const auto& jobConfig : *jobArray) {
                // todo: verify configuration
                Job job{
                    *jobConfig->get_as<uint8_t>("id"),
                    *jobConfig->get_as<std::string>("name"),
                    *jobConfig->get_as<uint8_t>("ht"),
                    *jobConfig->get_as<uint8_t>("st"),
                    *jobConfig->get_as<uint8_t>("dx"),
                    *jobConfig->get_as<uint8_t>("iq"),
                    *jobConfig->get_as<uint32_t>("start_hp"),
                    *jobConfig->get_as<uint32_t>("start_sp"),
                    *jobConfig->get_as<uint16_t>("hp_per_ht"),
                    *jobConfig->get_as<uint16_t>("hp_per_level"),
                    *jobConfig->get_as<uint16_t>("sp_per_iq"),
                    *jobConfig->get_as<uint16_t>("sp_per_level"),
                };

                CORE_LOGGING(trace) << "Loaded job " << static_cast<uint16_t>(job.id) << " " << job.name;
                _jobs.emplace(job.id, std::move(job));
            }
        } catch (const cpptoml::parse_exception& e) {
            CORE_LOGGING(warning) << "Failed to load jobs: " << e.what();
        }
    }

    const Job& JobManager::GetJob(uint8_t jobId) const { return _jobs.at(jobId); }
}  // namespace game::formats