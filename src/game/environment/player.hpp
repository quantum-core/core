// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>
#include <string>

#include "../../core/event/event.hpp"
#include "../../core/event/event_system.hpp"
#include "../../core/networking/connection.hpp"
#include "../../core/cache/player_cache.hpp"
#include "../item/inventory.hpp"
#include "object.hpp"

namespace game::environment {
    enum ChatTypes { CHAT_NORMAL = 0, CHAT_INFO = 1, CHAT_GROUP = 3, CHAT_GUILD = 4, CHAT_COMMAND = 5, CHAT_SHOUT = 6 };

    enum MovementTypes { WAIT = 0, MOVE = 1, ATTACK = 2, COMBO = 3, MOB_SKILL = 4, MAX, SKILL = 0x80 };

    enum Points {
        POINT_LEVEL = 1,
        POINT_HP = 5,
        POINT_MAX_HP = 6,
        POINT_SP = 7,
        POINT_MAX_SP = 8,
        POINT_GOLD = 11,
        POINT_ST = 12,
        POINT_HT = 13,
        POINT_DX = 14,
        POINT_IQ = 15,
        POINT_ATTACK_SPEED = 17,
        POINT_ATTACK_DAMAGE = 18,
        POINT_MOVE_SPEED = 19,
        POINT_MIN_ATTACK_DAMAGE = 29,
        POINT_MAX_ATTACK_DAMAGE = 30,
        POINT_MIN_WEAPON_DAMAGE = 200,
        POINT_MAX_WEAPON_DAMAGE = 201
    };

    enum PlayerClasses {
        CLASS_WARRIOR_MALE = 0,
        CLASS_SURA_MALE = 2,
        CLASS_NINJA_FEMALE = 1,
        CLASS_SHAMAN_FEMALE = 3,
        CLASS_WARRIOR_FEMALE = 4,
        CLASS_SURA_FEMALE = 6,
        CLASS_NINJA_MALE = 5,
        CLASS_SHAMAN_MALE = 7,
        CLASS_LYCAN_MALE = 8,
    };

    enum PlayerParts {
        PART_BODY,
        PART_WEAPON,
        PART_UNKNOWN,
        PART_HAIR,
    };

    enum PlayerState { IDLING, MOVING };

    class Player : public Object, public std::enable_shared_from_this<Player> {
       public:
        Player(uint32_t id, uint32_t vid, std::shared_ptr<core::networking::Connection> connection);
        virtual ~Player();

        [[nodiscard]] std::shared_ptr<core::networking::Connection> GetConnection() { return _connection; }
        void SetConnection(std::shared_ptr<core::networking::Connection> &&connection) {
            _connection = std::move(connection);
        }

        [[nodiscard]] uint32_t GetID() { return _id; }

        void Logout(bool close = false);

        void Load(const std::function<void()> &cb);
        void Show(const std::shared_ptr<core::networking::Connection> &connection) override;
        void Remove(const std::shared_ptr<core::networking::Connection> &connection) override;
        void Update(uint32_t elapsedTime) override;

        void TakeDamage(const std::shared_ptr<Object> &object, int damage) override;

        std::shared_ptr<Object> GetPointer() override { return shared_from_this(); }

        int32_t GetHP() override;
        int32_t GetMaxHP() override;

        void OnSpawned() override;
        void ObjectEnteredView(std::shared_ptr<Object> object) override;
        void ObjectLeftView(std::shared_ptr<Object> object) override;

        ObjectType GetObjectType() const override { return PLAYER; }

        void Persist();

        void Move(int32_t x, int32_t y);
        void Goto(int32_t x, int32_t y);
        void Teleport(int32_t x, int32_t y);

        uint8_t GetClassStatusBonus() const;
        uint32_t GetHitRate() const;
        uint32_t CalculateAttackDamage(uint32_t weaponDamage) const;
        uint32_t GetPoint(uint8_t point) const;
        void SetPoint(uint8_t point, uint32_t value);

        void Attack(const std::shared_ptr<Object> &target);
        void Dead() override;
        void Respawn(bool town = false);

        std::shared_ptr<item::Item> GetItem(uint8_t window, uint16_t position);
        bool HasSpace(uint8_t window, uint16_t position, uint8_t size);
        bool HasSpace(uint8_t size);
        bool RemoveItem(const std::shared_ptr<item::Item> &item, bool send = true);
        bool SetItem(uint8_t window, uint16_t position, const std::shared_ptr<item::Item> &item, bool send = true);
        bool GiveItem(const std::shared_ptr<item::Item> &item);
        bool FindSpace(uint8_t window, uint8_t size, uint16_t &position);
        bool FindSpace(uint8_t size, uint16_t &position);
        bool SwapItem(const std::shared_ptr<item::Item> &item1,
                      const std::shared_ptr<item::Item> &item2);
        bool AddItem(const std::shared_ptr<item::Item> &item);
        bool AddItem(uint8_t window, const std::shared_ptr<item::Item> &item);
        bool EquipItem(const std::shared_ptr<item::Item> &item);
        void ClearWindow(uint8_t window);

        void SendBasicData();
        void SendPoints();
        void SendInventory();
        void SendCharacter(const std::shared_ptr<core::networking::Connection> &connection);
        void SendCharacterAdditional(const std::shared_ptr<core::networking::Connection> &connection);
        void SendCharacterUpdate(const std::shared_ptr<core::networking::Connection> &connection);
        void SendChatMessage(uint8_t messageType, const std::string &message);
        void SendTarget();

        void OnItemMove(const std::shared_ptr<core::networking::Packet> &packet);
        void OnItemUse(const std::shared_ptr<core::networking::Packet> &packet);

        void OnChat(const std::shared_ptr<core::networking::Packet> &packet);
        void OnMove(const std::shared_ptr<core::networking::Packet> &packet);
        void OnAttack(const std::shared_ptr<core::networking::Packet> &packet);
        void OnTarget(const std::shared_ptr<core::networking::Packet> &packet);
        void OnDrop(const std::shared_ptr<core::networking::Packet> &packet);
        void OnOverInItem(const std::shared_ptr<core::networking::Packet> &packet);

        void DebugView();
        std::string GetLanguage() { return ""; }

        void SendConnectionAround(const std::function<void(const std::shared_ptr<core::networking::Connection> &)> &function);

       public:
        [[nodiscard]] const std::string &GetName() const override { return _name; }

        [[nodiscard]] uint8_t GetLevel() const { return _level; }
        void SetLevel(uint8_t level) { _level = level; }

        void SetTarget(const std::shared_ptr<Object> &target) {
            _target = target;
            SendTarget();
        }

       private:
        uint32_t _persistTimer;

        uint32_t _id;
        std::shared_ptr<core::networking::Connection> _connection;

        item::Inventory _inventory;
        std::shared_ptr<item::Item> _body;
        std::shared_ptr<item::Item> _head;
        std::shared_ptr<item::Item> _shoes;
        std::shared_ptr<item::Item> _bracelet;
        std::shared_ptr<item::Item> _weapon;
        std::shared_ptr<item::Item> _necklace;
        std::shared_ptr<item::Item> _earrings;
        std::shared_ptr<item::Item> _costume;
        std::shared_ptr<item::Item> _hair;

        std::weak_ptr<Object> _target;

        PlayerState _state;
        int32_t _startX;
        int32_t _startY;
        int32_t _targetX;
        int32_t _targetY;
        uint32_t _movementDuration;
        uint32_t _movementStart;

        uint8_t _moveSpeed = 150;
        uint8_t _attackSpeed = 140;

        std::string _name;
        uint8_t _playerClass;
        uint8_t _skillGroup;
        uint32_t _playtime;
        uint8_t _level;
        uint32_t _exp;
        uint32_t _gold;
        uint8_t _st;
        uint8_t _ht;
        uint8_t _dx;
        uint8_t _iq;
        int64_t _hp;
        int64_t _mp;
        uint32_t _stamina;

        std::weak_ptr<core::event::Event> _logoutEvent;
        int _logoutTimer;
    };
}  // namespace game::environment