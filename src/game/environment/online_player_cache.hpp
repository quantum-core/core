// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>
#include <string>

#include "player.hpp"

namespace game::environment {

    class OnlinePlayerCache {
       public:
            void Add(std::string playerName, std::weak_ptr<Player> player);
            void Remove(std::string player);
            bool IsOnline(std::string name);
            std::weak_ptr<Player> Get(std::string name);

        protected:
            std::unordered_map<std::string, std::weak_ptr<Player>> _onlinePlayers;
    };

}
