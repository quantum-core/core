// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "world.hpp"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <fstream>

#include "../../core/logger.hpp"
#include "../../core/profiler.hpp"

namespace game::environment {
    World::World() : _grid(0, 0), _vidCounter(0), _groups() {}

    World::~World() = default;

    void World::Load() {
        PROFILE_FUNCTION();
        CORE_LOGGING(trace) << "Initializing game world";

        // Load spawn groups
        _groups.Load("data/groups.toml");

        // Keep track of upper bound for grid
        uint64_t maxX = 0;
        uint64_t maxY = 0;

        // Load atlasinfo
        std::ifstream fileStream("data/atlasinfo.txt");

        if (!fileStream.is_open()) {
            CORE_LOGGING(error) << "Failed to open data/atlasinfo.txt";
            std::exit(EXIT_FAILURE);
        }

        std::vector<std::string> details;
        for (std::string line; std::getline(fileStream, line);) {
            if (line.empty()) continue;  // skip empty lines

            boost::split(details, line, boost::is_any_of("\t "));
            details.erase(
                std::remove_if(details.begin(), details.end(), [](const std::string &str) { return str.empty(); }),
                details.end());
            assert(details.size() == 5);

            std::string name = details[0];
            uint64_t x = std::stoul(details[1]);
            uint64_t y = std::stoul(details[2]);
            auto width = static_cast<uint8_t>(std::stoul(details[3]));
            auto height = static_cast<uint8_t>(std::stoul(details[4]));

            if (x + width * UNIT_SIZE > maxX) maxX = x + width * UNIT_SIZE;
            if (y + height * UNIT_SIZE > maxY) maxY = y + height * UNIT_SIZE;

            auto map = std::make_shared<Map>(name, x, y, width, height, shared_from_this());
            map->Load();
            _maps[name] = map;

            details.clear();
        }

        _grid.Resize(maxX / UNIT_SIZE, maxY / UNIT_SIZE);

        for (const auto &map : _maps) {
            for (auto x = map.second->GetUnitX(); x < map.second->GetUnitX() + map.second->GetWidth(); x++) {
                for (auto y = map.second->GetUnitY(); y < map.second->GetUnitY() + map.second->GetHeight(); y++) {
#ifndef NDEBUG
                    assert(_grid.Get(x, y) == nullptr);
#endif
                    _grid.Set(x, y, map.second);
                }
            }
        }

        for (const auto &map : _maps) {
            map.second->InitialSpawn();
        }
    }

    void World::Update(uint32_t elapsedTime) {
        for (const auto &map : _maps) {
            map.second->Update(elapsedTime);
        }
    }

    std::shared_ptr<Map> World::GetMapAtLocation(uint64_t x, uint64_t y) {
        auto gridX = x / UNIT_SIZE;
        auto gridY = y / UNIT_SIZE;
        if (gridX > _grid.GetWidth()) return nullptr;
        if (gridY > _grid.GetHeight()) return nullptr;

        return _grid.Get(gridX, gridY);
    }

    std::shared_ptr<Map> World::GetMapByName(const std::string &name) {
        if (_maps.find(name) == _maps.end()) return nullptr;
        return _maps[name];
    }

    std::shared_ptr<Player> World::CreatePlayer(uint32_t characterId,
                                                std::shared_ptr<core::networking::Connection> connection) {
        std::lock_guard<std::mutex> lock(_vidMutex);

        auto vid = ++_vidCounter;
        return std::make_shared<Player>(characterId, vid, connection);
    }

    std::shared_ptr<Monster> World::CreateMonster(uint32_t id, int32_t x, int32_t y, float rotation) {
        std::lock_guard<std::mutex> lock(_vidMutex);

        auto vid = ++_vidCounter;
        return std::make_shared<Monster>(id, vid, x, y, rotation);
    }

    void World::SpawnObject(const std::shared_ptr<Object> &object) {
        auto map = GetMapAtLocation(object->GetPositionX(), object->GetPositionY());
        if (!map) {
            CORE_LOGGING(error) << "Failed to spawn object at position " << object->GetPositionX() << ","
                                << object->GetPositionY() << " no map found!";
            return;
        }

        map->SpawnObject(object);
        object->SetMap(map);
    }
}  // namespace game::environment
