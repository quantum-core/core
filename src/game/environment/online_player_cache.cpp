// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "online_player_cache.hpp"

namespace game::environment {
    void OnlinePlayerCache::Add(std::string playerName, std::weak_ptr<Player> player) {
        std::transform(playerName.begin(), playerName.end(), playerName.begin(), ::tolower);
        _onlinePlayers.insert_or_assign(playerName, player);
    }

    void OnlinePlayerCache::Remove(std::string player) {
        std::transform(player.begin(), player.end(), player.begin(), ::tolower);
        _onlinePlayers.erase(player);
    }

    std::weak_ptr<Player> OnlinePlayerCache::Get(std::string player) {
        std::transform(player.begin(), player.end(), player.begin(), ::tolower);
        auto it = _onlinePlayers.find(player);
        return it != _onlinePlayers.end() ? it->second : std::weak_ptr<Player>();
    }

    bool OnlinePlayerCache::IsOnline(std::string player) { 
        std::transform(player.begin(), player.end(), player.begin(), ::tolower);
        return _onlinePlayers.find(player) == _onlinePlayers.end();
    }

}  // namespace game::environment
