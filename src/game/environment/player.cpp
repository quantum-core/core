// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "player.hpp"

#include <utility>

#include "../../core/language.hpp"
#include "../../core/logger.hpp"
#include "../../core/networking/utils.hpp"
#include "../../core/profiler.hpp"
#include "../../core/utils/math.hpp"
#include "../application.hpp"
#include "../commands/command_manager.hpp"

using namespace core::cache;

namespace game::environment {
    Player::Player(uint32_t id, uint32_t vid, std::shared_ptr<core::networking::Connection> connection)
        : Object(vid),
          _persistTimer(0),
          _id(id),
          _targetX(0),
          _targetY(0),
          _state(PlayerState::IDLING),
          _connection(std::move(connection)),
          _inventory(5, 9, 2),
          _name(),
          _playerClass(),
          _skillGroup(),
          _playtime(),
          _level(),
          _exp(),
          _gold(),
          _st(),
          _ht(),
          _dx(),
          _iq(),
          _hp(),
          _mp(),
          _stamina() {
        
    }

    Player::~Player() {
        Application::GetInstance()->GetOnlinePlayerCache()->Remove(_name);
        Application::GetInstance()->GetCoreApplication()->GetEventSystem()->CancelEvent(_logoutEvent);

        Persist();
        CORE_LOGGING(trace) << "Destroy player " << _name << " (" << _vid << ")";
    }

    void Player::Load(const std::function<void()> &cb) {
        auto core = game::Application::GetInstance();

        core->GetPlayerCache()->GetPlayer(_id, [this, core, cb](auto data) {
            _name = data.name;
            _playerClass = data.playerClass;
            _skillGroup = data.skillGroup;
            _playtime = data.playtime;
            _level = data.level;
            _exp = data.exp;
            _gold = data.gold;
            _st = data.st;
            _ht = data.ht;
            _dx = data.dx;
            _iq = data.iq;
            SetPositionX(data.posX);
            SetPositionY(data.posY);
            _hp = data.hp;
            _mp = data.mp;
            _stamina = data.stamina;

            if (data.teleportationX > 0 || data.teleportationY > 0) {
                SetPositionX(data.teleportationX);
                SetPositionY(data.teleportationY);
            }

            _inventory.Load(_id, 1, core->GetItemManager(), [this, cb, core]() {
                core->GetItemManager()->QueryItems(_id, 2, [this, cb](auto items) {
                    for (const auto &item : *items) {
                        SetItem(item->GetWindow(), item->GetPosition(), item, false);
                    }

                    SetPoint(POINT_HP, GetPoint(POINT_MAX_HP));
                    cb();
                });
            });
        });

        std::weak_ptr<Player> ptr;
        ptr = shared_from_this();
        Application::GetInstance()->GetOnlinePlayerCache()->Add(_name, ptr);
    }

    int32_t Player::GetHP() { return 0; }
    int32_t Player::GetMaxHP() { return 0; }

    void Player::SendBasicData() {
        auto character = _connection->GetServer()->GetPacketManager()->CreatePacket(0x71, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetString("name", _name);
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<int32_t>("posX", GetPositionX());
        character->SetField<int32_t>("posY", GetPositionY());
        character->SetField<uint8_t>("empire", 3);
        _connection->Send(character);
    }

    void Player::SendPoints() {
        auto points = _connection->GetServer()->GetPacketManager()->CreatePacket(0x10, core::networking::Outgoing);
        for (auto i = 0; i < 255; i++) {
            points->SetRepeatedField<uint32_t>("points", i, GetPoint(i));
        }
        _connection->Send(points);
    }

    void Player::SendInventory() {
        for (auto &item : _inventory.GetItems()) {
            item.second->Send(_connection);
        }

        if (_body) _body->Send(_connection);
        if (_head) _head->Send(_connection);
        if (_shoes) _shoes->Send(_connection);
        if (_bracelet) _bracelet->Send(_connection);
        if (_weapon) _weapon->Send(_connection);
        if (_necklace) _necklace->Send(_connection);
        if (_earrings) _earrings->Send(_connection);
        if (_costume) _costume->Send(_connection);
        if (_hair) _hair->Send(_connection);
    }

    void Player::SendCharacter(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        auto character = connection->GetServer()->GetPacketManager()->CreatePacket(0x01, core::networking::Outgoing);
        character->SetField<uint32_t>("vid", _vid);
        character->SetField<uint8_t>("characterType", GetObjectType());
        character->SetField<float>("angle", GetRotation());
        character->SetField<int32_t>("x", GetPositionX());
        character->SetField<int32_t>("y", GetPositionY());
        character->SetField<uint16_t>("class", _playerClass);
        character->SetField<uint8_t>("moveSpeed", _moveSpeed);
        character->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(character);
    }

    void Player::SendCharacterAdditional(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        CORE_LOGGING(trace) << "Send additional for " << _name << " (" << _vid << ")";

        auto additional = connection->GetServer()->GetPacketManager()->CreatePacket(0x88, core::networking::Outgoing);
        additional->SetField<uint32_t>("vid", _vid);
        additional->SetString("name", _name);
        additional->SetField<uint8_t>("empire", 3);
        additional->SetField<uint32_t>("level", _level);
        additional->SetRepeatedField<uint16_t>(
            "parts", PART_BODY, _costume ? _costume->GetProto().id : (_body ? _body->GetProto().id : 0));
        additional->SetRepeatedField<uint16_t>("parts", PART_WEAPON, _weapon ? _weapon->GetProto().id : 0);
        additional->SetRepeatedField<uint16_t>("parts", PART_UNKNOWN, 0);
        additional->SetRepeatedField<uint16_t>("parts", PART_HAIR, _hair ? _hair->GetProto().id : 0);
        connection->Send(additional);
    }

    void Player::SendCharacterUpdate(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        auto update = connection->GetServer()->GetPacketManager()->CreatePacket(0x13, core::networking::Outgoing);
        update->SetField<uint32_t>("vid", _vid);
        update->SetRepeatedField<uint16_t>("parts", PART_BODY, _costume ? _costume->GetProto().id : (_body ? _body->GetProto().id : 0));
        update->SetRepeatedField<uint16_t>("parts", PART_WEAPON, _weapon ? _weapon->GetProto().id : 0);
        update->SetRepeatedField<uint16_t>("parts", PART_UNKNOWN, 0);
        update->SetRepeatedField<uint16_t>("parts", PART_HAIR, _hair ? _hair->GetProto().id : 0);
        update->SetField<uint8_t>("moveSpeed", _moveSpeed);
        update->SetField<uint8_t>("attackSpeed", _attackSpeed);
        connection->Send(update);
    }

    void Player::Show(const std::shared_ptr<core::networking::Connection> &connection) {
        if (_dead) return;

        SendCharacter(connection);
        SendCharacterAdditional(connection);
    }

    void Player::Remove(const std::shared_ptr<core::networking::Connection> &connection) {
        auto packet =
            connection->GetServer()->GetPacketManager()->CreatePacket(0x02, core::networking::Direction::Outgoing);
        packet->SetField("vid", GetVID());
        connection->Send(packet);
    }

    std::shared_ptr<item::Item> Player::GetItem(uint8_t window, uint16_t position) {
        switch (window) {
            case WINDOW_INVENTORY:
                return _inventory.Get(position);
            case WINDOW_EQUIPMENT:
                switch (position) {
                    case EQUIP_BODY:
                        return _body;
                    case EQUIP_HEAD:
                        return _head;
                    case EQUIP_SHOES:
                        return _shoes;
                    case EQUIP_BRACELET:
                        return _bracelet;
                    case EQUIP_WEAPON:
                        return _weapon;
                    case EQUIP_NECKLACE:
                        return _necklace;
                    case EQUIP_EARRING:
                        return _earrings;
                    case EQUIP_COSTUME:
                        return _costume;
                    case EQUIP_HAIR:
                        return _hair;
                    default:
                        return nullptr;
                }
            default:
                return nullptr;
        }
    }

    bool Player::HasSpace(uint8_t window, uint16_t position, uint8_t size) {
        switch (window) {
            case WINDOW_INVENTORY:
                return _inventory.HasSpace(position, size);
            case WINDOW_EQUIPMENT:
                switch (position) {
                    case EQUIP_BODY:
                        return _body == nullptr;
                    case EQUIP_HEAD:
                        return _head == nullptr;
                    case EQUIP_SHOES:
                        return _shoes == nullptr;
                    case EQUIP_BRACELET:
                        return _bracelet == nullptr;
                    case EQUIP_WEAPON:
                        return _weapon == nullptr;
                    case EQUIP_NECKLACE:
                        return _necklace == nullptr;
                    case EQUIP_EARRING:
                        return _earrings == nullptr;
                    case EQUIP_COSTUME:
                        return _costume == nullptr;
                    case EQUIP_HAIR:
                        return _hair == nullptr;
                    default:
                        return false;
                }
            default:
                CORE_LOGGING(error) << "Player::HasSpace Unknown window " << static_cast<uint16_t>(window);
                return false;
        }
    }

    bool Player::AddItem(const std::shared_ptr<item::Item> &item) {
        return AddItem(WINDOW_INVENTORY, item);
    }

    bool Player::AddItem(uint8_t window, const std::shared_ptr<item::Item> &item) {
        uint16_t newPos, oldPos = item->GetPosition();
        uint8_t oldWin = item->GetWindow();

        if (!FindSpace(window, item->GetProto().size, newPos)) {
            CORE_LOGGING(error) << "Cannot add item, window full";
            return false;
        }

        if (!RemoveItem(item)) {
            CORE_LOGGING(error) << "Failed to remove item!";
            return false;
        }

        if (!SetItem(window, newPos, item)) {
            CORE_LOGGING(error) << "Failed to set item!";
            SetItem(oldWin, oldPos,
                    item);  // set item back to original position!
            return false;
        }

        item->Persist();

        if (window != oldWin) {
            auto itemManager = game::Application::GetInstance()->GetItemManager();
            itemManager->RemoveFromWindow(oldWin, _id, item->GetId());
            itemManager->AddToWindow(window, _id, item->GetId());
        }

        return true;
    }

    bool Player::EquipItem(const std::shared_ptr<item::Item>& item) {
        uint32_t antiFlags = item->GetProto().antiFlags;

        if (item->GetProto().limits[0].type == item::LIMIT_FLAG_LEVEL) {
            if (item->GetProto().limits[0].value > _level) {
                SendChatMessage(game::environment::CHAT_INFO, TRP(this, "You must be at a higher level to equip this item."));
                return false;
            }
        }

        if (item->GetProto().limits[1].type == item::LIMIT_FLAG_LEVEL) {
            if (item->GetProto().limits[1].value > _level) {
                SendChatMessage(game::environment::CHAT_INFO,
                                TRP(this, "You must be at a higher level to equip this item."));
                return false;
            }
        }

        switch (_playerClass) {
            case CLASS_WARRIOR_MALE: {
                if (antiFlags & item::ANTI_FLAG_WARRIOR) {
                    SendChatMessage(game::environment::CHAT_INFO,
                                    TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_MALE) {
                    SendChatMessage(game::environment::CHAT_INFO,
                                    TRP(this, "This item is only equippable to females."));
                    return false;
                }

                break;
            }
            case CLASS_WARRIOR_FEMALE: {
                if (antiFlags & item::ANTI_FLAG_WARRIOR) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_FEMALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to males."));
                    return false;
                }

                break;
            }
            case CLASS_NINJA_FEMALE: {
                if (antiFlags & item::ANTI_FLAG_NINJA) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_FEMALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to males."));
                    return false;
                }

                break;
            }
            case CLASS_NINJA_MALE: {
                if (antiFlags & item::ANTI_FLAG_NINJA) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_MALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to females."));
                    return false;
                }

                break;
            }
            case CLASS_SURA_FEMALE: {
                if (antiFlags & item::ANTI_FLAG_SURA) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_FEMALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to males."));
                    return false;
                }

                break;
            }
            case CLASS_SURA_MALE: {
                if (antiFlags & item::ANTI_FLAG_SURA) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_MALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to females."));
                    return false;
                }

                break;
            }
            case CLASS_SHAMAN_FEMALE: {
                if (antiFlags & item::ANTI_FLAG_SHAMAN) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_FEMALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to males."));
                    return false;
                }

                break;
            }
            case CLASS_SHAMAN_MALE: {
                if (antiFlags & item::ANTI_FLAG_SHAMAN) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_MALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to females."));
                    return false;
                }

                break;
            }
            case CLASS_LYCAN_MALE: {
                if (antiFlags & item::ANTI_FLAG_LYCAN) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is not for your class."));
                    return false;
                }

                if (antiFlags & item::ANTI_FLAG_MALE) {
                    SendChatMessage(game::environment::CHAT_INFO, TRP(this, "This item is only equippable to females."));
                    return false;
                }

                break;
            }

            default:
                return false;
        }

        // ��todo: capture flag and process this 3 antiflags
        //  ANTI_FLAG_SHINSOO = 512, ANTI_FLAG_CHUNJO = 1024, ANT_FLAG_JINNO = 2048,

        uint8_t equipPos = 0;
        bool canEquip = true;

        switch (item->GetProto().wearFlags) {
            case item::WEAR_FLAG_NONE:
                canEquip = false;
                break;
            case item::WEAR_FLAG_BODY:
                equipPos = EQUIP_BODY;
                break;
            case item::WEAR_FLAG_HEAD:
                equipPos = EQUIP_HEAD;
                break;
            case item::WEAR_FLAG_SHOES:
                equipPos = EQUIP_SHOES;
                break;
            case item::WEAR_FLAG_BRACELETS:
                equipPos = EQUIP_BRACELET;
                break;
            case item::WEAR_FLAG_EARRING:
                equipPos = EQUIP_EARRING;
                break;
            case item::WEAR_FLAG_NECKLACE:
                equipPos = EQUIP_NECKLACE;
                break;
            case item::WEAR_FLAG_WEAPON:
                equipPos = EQUIP_WEAPON;
                break;
            default:
                CORE_LOGGING(trace) << "Unknown wear flag " << item->GetProto().wearFlags;
                return false;
        }

        if (!canEquip) {
            switch (item->GetProto().type) {
                case item::ITEM_COSTUME:
                    switch (item->GetProto().subtype) {
                        case item::COSTUME_TYPE_BODY:
                            equipPos = EQUIP_COSTUME;
                            break;
                        case item::COSTUME_TYPE_HAIR:
                            equipPos = EQUIP_HAIR;
                            break;
                        default:
                            CORE_LOGGING(trace) << "Unknown costume subtype " << (uint16_t)item->GetProto().subtype;
                            return false;
                    }
                    break;
                default:
                    CORE_LOGGING(trace) << "Unknown item type " << (uint16_t)item->GetProto().type;
                    return false;
            }
        }

        auto item2 = GetItem(WINDOW_EQUIPMENT, equipPos);

        if (item2) {
            if (!SwapItem(item, item2)) {
                return false;
            }

            SendConnectionAround([this](const std::shared_ptr<core::networking::Connection> &c) { this->SendCharacterUpdate(c); });

            return true;
        }

        uint8_t oldPos = item->GetPosition();
        if (!RemoveItem(item)) {
            CORE_LOGGING(error) << "Failed to remove item!";
            return false;
        }

        if (!SetItem(WINDOW_EQUIPMENT, equipPos, item)) {
            CORE_LOGGING(error) << "Failed to set item";
            SetItem(WINDOW_INVENTORY, oldPos, item);
            return false;
        }

        item->Persist();

        auto itemManager = game::Application::GetInstance()->GetItemManager();
        itemManager->RemoveFromWindow(WINDOW_INVENTORY, _id, item->GetId());
        itemManager->AddToWindow(WINDOW_EQUIPMENT, _id, item->GetId());

        SendConnectionAround(
            [this](const std::shared_ptr<core::networking::Connection> &c) { this->SendCharacterUpdate(c); });

        return true;
    }

    bool Player::SwapItem(const std::shared_ptr<item::Item>& item1, const std::shared_ptr<item::Item>& item2) {
        uint16_t pos1 = item1->GetPosition(), pos2 = item2->GetPosition();
        uint8_t w1 = item1->GetWindow(), w2 = item2->GetWindow();

        // Remove item 1 so we can use the free slots in item1
        if (!RemoveItem(item1, false)) {
            CORE_LOGGING(error) << "Failed to remove item!";
            return false;
        }

        if (item1->GetProto().size < item2->GetProto().size) {
            // Is there enough space to swap item2 to item1 position?

            uint16_t npos = 0;
            if (!FindSpace(w1, item2->GetProto().size, npos)) {
                SendChatMessage(game::environment::CHAT_INFO, TRP(this, "No space in inventory."));
                SetItem(w1, pos1, item1, false); // Reset item
                return false;
            }

            item1->SendRemove(w1, pos1, _connection); // RemoveItem wipes the item position & window

            if (!RemoveItem(item2)) {
                CORE_LOGGING(error) << "Failed to remove item!";
                SetItem(w1, pos1, item1);
                return false;
            }

            if (!SetItem(w1, npos, item2)) {
                CORE_LOGGING(error) << "Failed to set item";
                SetItem(w1, pos1, item1);
                SetItem(w2, pos2, item2);
                return false;
            }
        } else {
            item1->SendRemove(w1, pos1, _connection);  // RemoveItem wipes the item position & window

            if (!RemoveItem(item2)) {
                CORE_LOGGING(error) << "Failed to remove item!";
                SetItem(w1, pos1, item1);
                return false;
            }

            if (!SetItem(w1, pos1, item2)) {
                CORE_LOGGING(error) << "Failed to set item";
                SetItem(w1, pos1, item1);
                SetItem(w2, pos2, item2);
                return false;
            }
        }

        if (!SetItem(w2, pos2, item1)) {
            CORE_LOGGING(error) << "Failed to set item!";
            SetItem(w2, pos2, item2);
            SetItem(w1, pos1, item1);
            return false;
        }

        item1->Persist();
        item2->Persist();

        if (w1 != w2) {
            auto itemManager = game::Application::GetInstance()->GetItemManager();
            itemManager->RemoveFromWindow(w1, _id, item1->GetId());
            itemManager->RemoveFromWindow(w2, _id, item2->GetId());
            itemManager->AddToWindow(w2, _id, item1->GetId());
            itemManager->AddToWindow(w1, _id, item2->GetId());
        }

        return true;
    }

    bool Player::RemoveItem(const std::shared_ptr<item::Item> &item, bool send) {
        if (!item) return false;
        auto window = item->GetWindow();
        auto position = item->GetPosition();

        switch (window) {
            case WINDOW_INVENTORY:
                _inventory.Clear(position);
                if (send) item->SendRemove(_connection);
                item->SetWindow(WINDOW_INVENTORY);
                item->SetPosition(0);
                return true;
            case WINDOW_EQUIPMENT:
                switch (position) {
                    case EQUIP_BODY:
                        _body = nullptr;
                        break;
                    case EQUIP_HEAD:
                        _head = nullptr;
                        break;
                    case EQUIP_SHOES:
                        _shoes = nullptr;
                        break;
                    case EQUIP_BRACELET:
                        _bracelet = nullptr;
                        break;
                    case EQUIP_WEAPON:
                        _weapon = nullptr;
                        break;
                    case EQUIP_NECKLACE:
                        _necklace = nullptr;
                        break;
                    case EQUIP_EARRING:
                        _earrings = nullptr;
                        break;
                    case EQUIP_HAIR:
                        _hair = nullptr;
                        break;
                    case EQUIP_COSTUME:
                        _costume = nullptr;
                        break;
                    default:
                        return false;
                }

                if (send) {
                    item->SendRemove(_connection);
                    SendCharacterUpdate(_connection);  // todo send to view
                    SendPoints();
                    SendConnectionAround([this](const std::shared_ptr<core::networking::Connection> &c) {
                        this->SendCharacterUpdate(c);
                    });
                }
                item->SetWindow(WINDOW_INVENTORY);
                item->SetPosition(0);
                return true;
            default:
                return false;
        }
    }

    bool Player::SetItem(uint8_t window, uint16_t position, const std::shared_ptr<item::Item> &item, bool send) {
        if (!item) return false;

        switch (window) {
            case WINDOW_INVENTORY:
                _inventory.Set(position, item);
                item->SetWindow(window);
                item->SetPosition(position);
                if (send) item->Send(_connection);
                return true;
            case WINDOW_EQUIPMENT:
                switch (position) {
                    case EQUIP_BODY:
                        _body = item;
                        break;
                    case EQUIP_HEAD:
                        _head = item;
                        break;
                    case EQUIP_SHOES:
                        _shoes = item;
                        break;
                    case EQUIP_BRACELET:
                        _bracelet = item;
                        break;
                    case EQUIP_WEAPON:
                        _weapon = item;
                        break;
                    case EQUIP_NECKLACE:
                        _necklace = item;
                        break;
                    case EQUIP_EARRING:
                        _earrings = item;
                        break;
                    case EQUIP_COSTUME:
                        _costume = item;
                        break;
                    case EQUIP_HAIR:
                        _hair = item;
                        break;
                    default:
                        return false;
                }

                item->SetWindow(window);
                item->SetPosition(position);
                if (send) {
                    item->Send(_connection);
                    SendCharacterUpdate(_connection);  // todo send to view
                    SendPoints();
                }
                return true;
            default:
                return false;
        }
    }

    bool Player::GiveItem(const std::shared_ptr<item::Item> &item) {
        auto size = item->GetProto().size;
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(WINDOW_INVENTORY, i, size)) {
                SetItem(WINDOW_INVENTORY, i, item);
                item->Persist();
                game::Application::GetInstance()->GetItemManager()->AddToWindow(1, _id, item->GetId());
                return true;
            }
        }
        return false;
    }

    bool Player::HasSpace(uint8_t size) {
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(WINDOW_INVENTORY, i, size)) {
                return true;
            }
        }
        return false;
    }

    bool Player::FindSpace(uint8_t window, uint8_t size, uint16_t &position) {
        for (auto i = 0; i < 90; i++) {
            if (HasSpace(window, i, size)) {
                position = i;
                return true;
            }
        }

        return false;
    }

    bool Player::FindSpace(uint8_t size, uint16_t &position) { 
        return FindSpace(WINDOW_INVENTORY, size, position);
    }

    void Player::ClearWindow(uint8_t window) {
        std::shared_ptr<item::Item> item;
        auto itemManager = game::Application::GetInstance()->GetItemManager();

        switch (window) {
            case WINDOW_INVENTORY:
                for (auto i = 0; i < 90; i++) {
                    item = GetItem(window, i);
                    if (item) {
                        RemoveItem(item);
                        item->Destroy();
                        itemManager->RemoveFromWindow(WINDOW_INVENTORY, _id, item->GetId());
                    }
                }
                break;
            case WINDOW_EQUIPMENT:
                item = _body;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _necklace;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                };
                item = _shoes;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _earrings;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _bracelet;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _head;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _weapon;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _costume;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                item = _hair;
                if (item) {
                    RemoveItem(item);
                    item->Destroy();
                    itemManager->RemoveFromWindow(WINDOW_EQUIPMENT, _id, item->GetId());
                }
                break;
            default:
                break;
        }
    }

    void Player::SendChatMessage(uint8_t messageType, const std::string &message) {
        auto packet =
            _connection->GetServer()->GetPacketManager()->CreatePacket(0x04, core::networking::Direction::Outgoing);
        packet->SetField<uint8_t>("messageType", messageType);
        packet->SetDynamicString(message);
        _connection->Send(packet);
    }

    void Player::Move(int32_t x, int32_t y) {
        SetPositionX(x);
        SetPositionY(y);
    }

    void Player::Goto(int32_t x, int32_t y) {
        if (GetPositionX() == x && GetPositionY() == y) return;  // we are already at our target
        if (_targetX == x && _targetY == y) return;              // we already have this position as our target

        // Get animation data
        auto animation = game::Application::GetInstance()->GetAnimationManager()->GetAnimation(
            _playerClass, formats::AnimationType::RUN, formats::AnimationSubType::GENERAL);
        if (!animation) {
            CORE_LOGGING(error) << "Failed to find animation run for " << _playerClass;
            return;
        }

        _state = PlayerState::MOVING;
        _targetX = x;
        _targetY = y;
        _startX = GetPositionX();
        _startY = GetPositionY();
        _movementStart = _connection->GetServer()->GetApplication()->GetCoreTime();

        float distance = core::utils::Math::Distance(_startX, _startY, _targetX, _targetY);
        float animationSpeed = -animation->accumulationY / animation->motionDuration;

        int i = 100 - _moveSpeed;
        if (i > 0) {
            i = 100 + i;
        } else if (i < 0) {
            i = 10000 / (100 - i);
        } else {
            i = 100;
        }
        int duration = static_cast<int>((distance / animationSpeed) * 1000) * i / 100;
        _movementDuration = duration;

        CORE_LOGGING(trace) << "Movement duration: " << _movementDuration;
    }

    void Player::Teleport(int32_t x, int32_t y) {
        auto map = GetMap();
        if (x >= map->GetX() && y >= map->GetY() && x < map->GetX() + map->GetWidth() * UNIT_SIZE &&
            y < map->GetY() + map->GetHeight() * UNIT_SIZE) {
            SetPositionX(x);
            SetPositionY(y);
            _state = PlayerState::IDLING;
            Show(_connection);
            return;
        }

        auto cache = game::Application::GetInstance()->GetPlayerCache();
        cache->SetTeleportationPosition(_id, x, y);

        // Calculate target server
        auto config = game::Application::GetInstance()->GetCoreApplication()->GetConfiguration();
        std::string localAddress;
        if (config.count("ip") != 0) {
            localAddress = config.get<std::string>("ip");
        } else {
            localAddress = core::networking::GetLocalAddress();
        }
        auto localIp = core::networking::ConvertIP(localAddress);

        auto p = _connection->GetServer()->GetPacketManager()->CreatePacket(0x41);
        p->SetField<int32_t>("x", x);
        p->SetField<int32_t>("y", y);
        p->SetField<int32_t>("address", localIp);
        p->SetField<uint16_t>("port", config.get<uint16_t>("port"));
        _connection->Send(p);
    }

    void Player::Update(uint32_t elapsedTime) {
        PROFILE_FUNCTION();

        if (!_map) return;  // We aren't fully spawned yet!

        // Increment persist timer and persist the player every second
        _persistTimer += elapsedTime;
        if (_persistTimer >= 1000) {
            Persist();
            _persistTimer -= 1000;
        }

        auto time = Application::GetInstance()->GetCoreApplication()->GetCoreTime();

        if (_state == PlayerState::MOVING) {
            auto elapsed = time - _movementStart;
            auto rate = elapsed / (float)_movementDuration;
            if (rate > 1) rate = 1;

            auto x = (int32_t)((float)(_targetX - _startX) * rate + _startX);
            auto y = (int32_t)((float)(_targetY - _startY) * rate + _startY);

            Move(x, y);

            if (rate >= 1) {
                _state = PlayerState::IDLING;
                CORE_LOGGING(trace) << "Movement of " << _name << " done";
            }
        }

        Object::Update(elapsedTime);
    }

    void Player::OnSpawned() {}

    void Player::ObjectEnteredView(std::shared_ptr<Object> object) {
        if (_connection) object->Show(_connection);
    }

    void Player::ObjectLeftView(std::shared_ptr<Object> object) {
        if (_connection) object->Remove(_connection);
    }

    void Player::Persist() {
        PROFILE_FUNCTION();

        auto core = game::Application::GetInstance();
        core->GetPlayerCache()->SavePlayer({_id,
                                            _name,
                                            _playerClass,
                                            _skillGroup,
                                            _playtime,
                                            _level,
                                            _exp,
                                            _gold,
                                            _st,
                                            _ht,
                                            _dx,
                                            _iq,
                                            GetPositionX(),
                                            GetPositionY(),
                                            0,
                                            0,
                                            _hp,
                                            _mp,
                                            _stamina,
                                            _costume ? _costume->GetProto().id : (_body ? _body->GetProto().id : 0),
                                            _hair ? _hair->GetProto().id : 0});
    }

    void Player::DebugView() {
        ForEachAround([this](auto obj) {
            auto distance =
                core::utils::Math::Distance(obj->GetPositionX(), obj->GetPositionY(), GetPositionX(), GetPositionY());
            SendChatMessage(1,
                            "[" + std::to_string(obj->GetVID()) + "]" + obj->GetName() +
                                " - Distance: " + std::to_string(distance));
        });

        SendChatMessage(1, "-------------------------");
        SendChatMessage(1, "Count: " + std::to_string(GetObjectsInViewCount()));
    }

    void Player::Logout(bool close) {
        _logoutTimer = 10;
        auto evtSystem = Application::GetInstance()->GetCoreApplication()->GetEventSystem();

        evtSystem->CancelEvent(_logoutEvent);
        _logoutEvent = evtSystem->EnqueueEvent(
            [this, close]() {
                if (_logoutTimer == 0) {
                    if (close) SendChatMessage(game::environment::CHAT_COMMAND, "quit");
                    GetConnection()->PostShutdown();

                    return 0;  // will cancel this event
                }

                SendChatMessage(game::environment::CHAT_INFO,
                                TR("You will be logged out in %d seconds.", _logoutTimer));

                _logoutTimer--;
                return 1000;
            },
            0);
    }

    void Player::SendConnectionAround(
        const std::function<void(const std::shared_ptr<core::networking::Connection> &)> &function) {
        ForEachAround([function](const std::shared_ptr<Object> &object) {
            if (object->GetObjectType() != PLAYER) return;

            const auto &player = std::static_pointer_cast<Player>(object);
            auto connection = player->GetConnection();
            if (connection) function(connection);
        });
    }
}  // namespace game::environment
