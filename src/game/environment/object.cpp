// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "object.hpp"

#include "../../core/utils/math.hpp"
#include "map.hpp"
#include "player.hpp"

namespace game::environment {
    void Object::SendPacketAround(const std::shared_ptr<core::networking::Packet> &packet) {
        ForEachAround([packet](const std::shared_ptr<Object> &object) {
            if (object->GetObjectType() != PLAYER) return;

            const auto &player = std::static_pointer_cast<Player>(object);
            auto connection = player->GetConnection();
            if (connection) connection->Send(packet);
        });
    }

    bool Filter(const std::shared_ptr<Object> &obj, const std::weak_ptr<Object> &w) {
        // auto w2 = w.lock();
        // return obj == w2;
        return !w.owner_before(obj) && !obj.owner_before(w);
    }

    void Object::Update(uint32_t elapsedTime) {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        // Check all objects in view for too far objects
        for (auto it = _viewObjects.begin(); it != _viewObjects.end();) {
            auto obj = it->second.lock();
            if (!obj) {
                it = _viewObjects.erase(it);
                continue;
            }

            auto distance =
                core::utils::Math::Distance(obj->GetPositionX(), obj->GetPositionY(), GetPositionX(), GetPositionY());
            if (distance > 10000) {
                ObjectLeftView(obj);
                obj->RemoveObjectFromView(GetPointer());

                it = _viewObjects.erase(it);
                continue;
            }

            it++;
        }

        if (GetObjectType() == PLAYER) {
            // Check for objects currently around
            std::vector<std::shared_ptr<Object>> objects;
            _map->QueryObjectsAround(objects, GetPositionX(), GetPositionY(), 10000);
            for (const auto &obj : objects) {
                if (obj.get() == this) continue;

                if (_viewObjects.find(obj->GetVID()) != _viewObjects.end()) continue;

                ObjectEnteredView(obj);
                obj->AddObjectToView(GetPointer());
                _viewObjects[obj->GetVID()] = obj;
            }
        }
    }
    void Object::ForEachAround(const std::function<void(const std::shared_ptr<Object> &)> &function) {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        for (const auto &objectptr : _viewObjects) {
            auto object = objectptr.second.lock();
            if (!object) continue;
            if (object.get() == this) continue;

            function(object);
        }
    }

    void Object::AddObjectToView(std::shared_ptr<Object> object) {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        _viewObjects[object->GetVID()] = object;
        ObjectEnteredView(object);
    }

    void Object::RemoveObjectFromView(std::shared_ptr<Object> object) {
        std::lock_guard<std::mutex> lock(_viewObjectsMutex);

        _viewObjects.erase(object->GetVID());
        ObjectLeftView(object);
    }

    void Object::AddTargetedBy(uint32_t vid, const std::weak_ptr<Player> &object) {
        std::lock_guard<std::mutex> lock(_targetedByMutex);
        _targetedBy[vid] = object;
    }

    void Object::RemoveTargetedBy(uint32_t vid) {
        std::lock_guard<std::mutex> lock(_targetedByMutex);
        _targetedBy.erase(vid);
    }

    void Object::RefreshTarget() {
        std::lock_guard<std::mutex> lock(_targetedByMutex);

        auto it = _targetedBy.begin();
        while (it != _targetedBy.end()) {
            auto player = it->second.lock();
            if (player) {
                player->SendTarget();
            } else {
                it = _targetedBy.erase(it);
                continue;
            }

            it++;
        }
    }

    void Object::ClearTarget() {
        std::lock_guard<std::mutex> lock(_targetedByMutex);

        for (const auto &it : _targetedBy) {
            auto player = it.second.lock();
            if (player) {
                player->SetTarget(nullptr);
            }
        }

        _targetedBy.clear();
    }
}  // namespace game::environment