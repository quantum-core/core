// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "../environment/player.hpp"

namespace game::commands {
    void CommandItem(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandSpawn(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandDebug(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandTeleport(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandClearWindow(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandSet(const std::shared_ptr<environment::Player> &player,
                            const std::vector<std::string> &arguments);
    void CommandKill(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);

    void CommandQuit(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandLogout(const std::shared_ptr<environment::Player> &player, const std::vector<std::string> &arguments);
    void CommandPhaseSelect(const std::shared_ptr<environment::Player> &player,
                            const std::vector<std::string> &arguments);
    void CommandRestartHere(const std::shared_ptr<environment::Player> &player,
                            const std::vector<std::string> &arguments);
    void CommandRestartTown(const std::shared_ptr<environment::Player> &player,
                            const std::vector<std::string> &arguments);
}  // namespace game::commands