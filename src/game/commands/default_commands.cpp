// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "default_commands.hpp"

#include "../../core/language.hpp"
#include "../../core/networking/reserved_headers.hpp"
#include "../application.hpp"
#include "../environment/map.hpp"

namespace game::commands {
    void CommandItem(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        if (arguments.size() < 2) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/item (player) [vnum] (count)");
            return;
        }

        std::shared_ptr<environment::Player> targetPlayer;
        uint32_t vnum;
        uint8_t count = 1;

        try {
            if (arguments.size() > 3) {
                vnum = boost::lexical_cast<uint32_t>(arguments[2]);
                count = static_cast<uint8_t>(boost::lexical_cast<uint16_t>(arguments[3]));

                auto playerPtr = Application::GetInstance()->GetOnlinePlayerCache()->Get(arguments[1]);
                auto p = playerPtr.lock();

                if (!p) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            TRP(player, "The player %s is not online", arguments[1]));
                    return;
                }

                targetPlayer = p;
            }
            else if (arguments.size() > 2) {
                try {
                    vnum = boost::lexical_cast<uint32_t>(arguments[1]);
                    count = static_cast<uint8_t>(boost::lexical_cast<uint16_t>(arguments[2]));
                    targetPlayer = player;
                } catch (boost::bad_lexical_cast& e) {
                    vnum = boost::lexical_cast<uint32_t>(arguments[2]);

                    auto playerPtr = Application::GetInstance()->GetOnlinePlayerCache()->Get(arguments[1]);
                    auto p = playerPtr.lock();

                    if (!p) {
                        player->SendChatMessage(game::environment::CHAT_INFO,
                                                TRP(player, "The player %s is not online", arguments[1]));
                        return;
                    }

                    targetPlayer = player;
                    count = 1;
                }
            } else {
                vnum = boost::lexical_cast<uint32_t>(arguments[1]);
                count = 1;
                targetPlayer = player;
            }

            auto itemManager = game::Application::GetInstance()->GetItemManager();
            if (!itemManager->HasProto(vnum)) {
                player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Item not found."));
                return;
            }

            auto proto = itemManager->GetProto(vnum);
            if (!targetPlayer->HasSpace(proto.size)) {
                player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Player %s No space in inventory.", targetPlayer->GetName()));
                return;
            }

            auto item = itemManager->CreateItem(vnum, player->GetID());

            if (count > 1) {
                item->Add(count - 1);
            }

            targetPlayer->GiveItem(item);
        } catch (boost::bad_lexical_cast& e) {
            CORE_LOGGING(trace) << "'" << arguments[1] << "'";
            player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Vnum must be an integer."));
        }
    }
    void CommandKill(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        if (arguments.size() < 2) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/kill [player]");
            return;      
        }

        auto playerPtr = Application::GetInstance()->GetOnlinePlayerCache()->Get(arguments[1]);
        auto p = playerPtr.lock();

        if (!p) {
            player->SendChatMessage(game::environment::CHAT_INFO,
                                    TRP(player, "The player %s is not online", arguments[1]));
            return;
        }

        p->Dead();
    }

    void CommandClearWindow(
        const std::shared_ptr<environment::Player>& player,
        const std::vector<std::string>& arguments) {
        if (arguments.size() < 2) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/clearwindow [all,equipment,inventory]");
            return;
        }

        auto str = arguments[1];
        if (str == "equipment") {
            player->ClearWindow(core::cache::WINDOW_EQUIPMENT);
        } else if (str == "inventory") {
            player->ClearWindow(core::cache::WINDOW_INVENTORY);
        } else if (str == "all") {
            player->ClearWindow(core::cache::WINDOW_EQUIPMENT);
            player->ClearWindow(core::cache::WINDOW_INVENTORY);
        } else  {
            player->SendChatMessage(environment::CHAT_INFO, TRP(player, "Invalid argument"));
        }
    }
    void CommandSet(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        if (arguments.size() < 2) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/set [gold,level] [value]");
            return;
        }

        uint32_t val;
        std::string type;
        std::shared_ptr<environment::Player> targetPlayer;

        try {
            if (arguments.size() > 2) {
                auto playerPtr = Application::GetInstance()->GetOnlinePlayerCache()->Get(arguments[1]);
                auto p = playerPtr.lock();

                if (!p) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            TRP(player, "The player %s is not online", arguments[1]));
                    return;
                }

                targetPlayer = p;
                val = boost::lexical_cast<uint32_t>(arguments[3]);
                type = arguments[2];
            } else {
                val = boost::lexical_cast<uint32_t>(arguments[2]);
                type = arguments[1];
                targetPlayer = player;
            }

            if (type == "gold") {
                targetPlayer->SetPoint(game::environment::POINT_GOLD, val);
            } else if (type == "level") {
                targetPlayer->SetPoint(game::environment::POINT_LEVEL, val);
            } else {
                player->SendChatMessage(environment::CHAT_INFO, TRP(player, "Invalid argument"));
                return;
            }

            targetPlayer->SendPoints();


        } catch (boost::bad_lexical_cast&) {
            CORE_LOGGING(trace) << "'" << arguments[1] << "'";
            player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Value must be an integer."));
        }
    }

    void CommandSpawn(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {}
    void CommandDebug(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        if (arguments.size() < 2) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/debug [view|position]");
            return;
        }

        if (arguments[1] == "view") {
            player->DebugView();
        }

        if (arguments[1] == "object_count") {
            auto count = player->GetMap()->GetObjectCount();
            player->SendChatMessage(game::environment::CHAT_INFO, "Objects: " + std::to_string(count));
        }

        if (arguments[1] == "position") {
            player->SendChatMessage(game::environment::CHAT_INFO,
                                    "Global Position: " + std::to_string(player->GetPositionX()) + ", " +
                                        std::to_string(player->GetPositionY()));

            auto localX = player->GetPositionX() - player->GetMap()->GetX();
            auto localY = player->GetPositionY() - player->GetMap()->GetY();
            player->SendChatMessage(game::environment::CHAT_INFO,
                                    "Global Position: " + std::to_string(localX) + ", " + std::to_string(localY));
        }

        if (arguments[1] == "point") {
            if (arguments.size() < 3) {
                return;
            }

            player->SendChatMessage(game::environment::CHAT_INFO,
                                    std::to_string(player->GetPoint(boost::lexical_cast<uint32_t>(arguments[2]))));
        }
    }
    void CommandTeleport(const std::shared_ptr<environment::Player>& player,
                         const std::vector<std::string>& arguments) {
        if (arguments.size() != 2 && arguments.size() != 3) {
            player->SendChatMessage(game::environment::CHAT_INFO, "/teleport [map]");
            player->SendChatMessage(game::environment::CHAT_INFO, "/teleport [x] [y]");
            return;
        }

        if (arguments.size() == 2) {
            auto map = game::Application::GetInstance()->GetWorld()->GetMapByName(arguments[1]);
            if (!map) {
                player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Invalid map provided."));
                return;
            }

            player->Teleport(map->GetX() + map->GetWidth() * UNIT_SIZE / 2,
                             map->GetY() + map->GetHeight() * UNIT_SIZE / 2);
        } else if (arguments.size() == 3) {
            auto map = player->GetMap();
            try {
                auto x = boost::lexical_cast<int32_t>(arguments[1]) * 100;
                auto y = boost::lexical_cast<int32_t>(arguments[2]) * 100;

                player->Teleport(map->GetX() + x, map->GetY() + y);
            } catch (const boost::bad_lexical_cast& e) {
                player->SendChatMessage(game::environment::CHAT_INFO,
                                        TRP(player, "The coordinates have to be an integer."));
            }
        }
    }

    void CommandQuit(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        player->Logout(true);
    }
    void CommandLogout(const std::shared_ptr<environment::Player>& player, const std::vector<std::string>& arguments) {
        player->Logout();
    }
    void CommandPhaseSelect(const std::shared_ptr<environment::Player>& player,
                            const std::vector<std::string>& arguments) {
        auto connection = player->GetConnection();

        connection->SetPlayer(nullptr);

        player->SetConnection(nullptr);
        auto map = player->GetMap();
        if (map) {
            map->DespawnObject(player);
        }

        connection->SetPhase(core::networking::Phases::PHASE_SELECT);
    }
    void CommandRestartHere(const std::shared_ptr<environment::Player>& player,
                            const std::vector<std::string>& arguments) {
        player->Respawn();
    }
    void CommandRestartTown(const std::shared_ptr<environment::Player>& player,
                            const std::vector<std::string>& arguments) {
        player->Respawn(true);
    }
}  // namespace game::commands