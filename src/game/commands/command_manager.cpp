// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "command_manager.hpp"

#include <utility>

#include "../../core/language.hpp"
#include "../../core/logger.hpp"
#include "../application.hpp"
#include "default_commands.hpp"

namespace game::commands {
    std::vector<Command> CommandManager::_commands;

    void CommandManager::RegisterCommand(const std::string &name, CommandFunction &&func) {
        Command command{name, std::move(func)};
        _commands.push_back(command);
    }

    void CommandManager::Execute(const std::shared_ptr<environment::Player> &player, const std::string &command) {
        if (!player) return;

        std::stringstream stream(command.substr(1));
        std::string segment;
        std::vector<std::string> arguments;

        while (std::getline(stream, segment, ' ')) {
            arguments.push_back(segment);
        }

        auto commandName = arguments.front();
        for (const auto &command : _commands) {
            if (command.name == commandName) {
                command.func(player, arguments);
                return;
            }
        }

        CORE_LOGGING(trace) << "Invalid command " << commandName;
        player->SendChatMessage(game::environment::CHAT_INFO, TRP(player, "Command not found."));
    }

    void CommandManager::RegisterDefaultCommands() {
        RegisterCommand("teleport", &CommandTeleport);
        RegisterCommand("item", &CommandItem);
        RegisterCommand("debug", &CommandDebug);
        RegisterCommand("quit", &CommandQuit);
        RegisterCommand("logout", &CommandLogout);
        RegisterCommand("phase_select", &CommandPhaseSelect);
        RegisterCommand("restart_here", &CommandRestartHere);
        RegisterCommand("restart_town", &CommandRestartTown);
        RegisterCommand("spawn", &CommandSpawn);
        RegisterCommand("clearwindow", &CommandClearWindow);
        RegisterCommand("set", &CommandSet);
        RegisterCommand("kill", &CommandKill);
    }
}  // namespace game::commands