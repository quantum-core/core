// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "item_manager.hpp"

#include "../../core/logger.hpp"
#include "../../core/mysql/migrations/_defaults.hpp"
#include "../../core/profiler.hpp"
#include "../../core/utils/async_helper.hpp"
#include "item.hpp"

namespace game::item {
    ItemManager::ItemManager(std::shared_ptr<core::cache::Redis> redis, std::shared_ptr<core::mysql::MySQL> database)
        : _protos(), _redis(std::move(redis)), _database(std::move(database)) {}

    ItemManager::~ItemManager() = default;

    void ItemManager::Load() {
        PROFILE_FUNCTION();
        formats::ItemProto proto;
        if (!proto.Load("data/item_proto", _protos)) {
            return;
        }

        CORE_LOGGING(info) << "Loaded " << _protos.size() << " items";
    }

    const formats::Item &ItemManager::GetProto(uint32_t id) const { return _protos.at(id); }

    bool ItemManager::HasProto(uint32_t id) const { return _protos.count(id) != 0; }

    std::shared_ptr<Item> ItemManager::CreateItem(uint32_t vnum, uint32_t owner) {
        // Generate item id
        auto id = _redis->Execute<int64_t>(bredis::single_command_t{"incr", "item_counter"});
        auto item = std::make_shared<Item>(shared_from_this(), _protos[vnum], owner, id, 1);
        item->Persist(true);

        return item;
    }

    void ItemManager::GetItemById(uint64_t id, const std::function<void(std::shared_ptr<Item>)> &cb) {
        Item::Load(shared_from_this(), id, cb);
    }

    void ItemManager::QueryItems(uint32_t playerId, uint16_t window,
                                 const std::function<void(std::shared_ptr<std::vector<std::shared_ptr<Item>>>)> &cb) {
        auto ret = std::make_shared<std::vector<std::shared_ptr<Item>>>();

        auto key = "items:" + std::to_string(playerId) + ":" + std::to_string(window);
        auto exists = _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});
        if (exists == 0) {
            // Load data from database
            auto stmt = _database->CreateStatement("SELECT `id` FROM `" GAME_DATABASE
                                                   "`.`items` WHERE `player_id` = %1% and `window` = %2%");
            stmt << playerId << window;

            stmt.Execute([this, cb, key](auto err, auto result) -> void {
                std::vector<uint64_t> itemIds;
                while (result->Next()) {
                    // Load the item from the cache or from the database
                    auto itemId = result->GetUnsigned64("id");
                    CORE_LOGGING(trace) << "Query item found: " << itemId;
                    itemIds.push_back(itemId);
                    _redis->Execute(bredis::single_command_t{"lpush", key, std::to_string(itemId)});
                }

                core::utils::QueryList<std::shared_ptr<Item>, uint64_t>(
                    [this](uint64_t id, auto cb) { GetItemById(id, cb); }, itemIds, cb);
            });
        } else {
            std::vector<uint64_t> itemIds;

            auto length = _redis->Execute<int64_t>(bredis::single_command_t{"llen", key});
            for (auto i = 0; i < length; i++) {
                auto id =
                    _redis->Execute<core::cache::string_t>(bredis::single_command_t{"lindex", key, std::to_string(i)});
                itemIds.push_back(boost::lexical_cast<int64_t>(id.str));
            }

            core::utils::QueryList<std::shared_ptr<Item>, uint64_t>(
                [this](uint64_t id, auto cb) { GetItemById(id, cb); }, itemIds, cb);
        }
    }

    void ItemManager::RemoveFromWindow(uint16_t window, uint32_t playerId, uint64_t id) {
        auto key = "items:" + std::to_string(playerId) + ":" + std::to_string(window);
        _redis->Execute(bredis::single_command_t{"lrem", key, "1", std::to_string(id)});
    }
    void ItemManager::AddToWindow(uint16_t window, uint32_t playerId, uint64_t id) {
        auto key = "items:" + std::to_string(playerId) + ":" + std::to_string(window);
        _redis->Execute(bredis::single_command_t{"rpush", key, std::to_string(id)});
    }
}  // namespace game::item