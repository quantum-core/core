// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>

#include "../../core/networking/connection.hpp"
#include "../formats/item_proto.hpp"
#include "item_manager.hpp"

namespace game::item {
    enum ItemTypes {
        ITEM_TYPE_WEAPON = 1,
        ITEM_TYPE_ARMOR = 2,
        ITEM_USABLE = 3,
        ITEM_COSTUME = 28,
    };

    // Subtypes
    enum ArmorTypes {
        ARMOR_TYPE_BODY = 0,
        ARMOR_TYPE_HEAD = 1,
        AMMOR_TYPE_BRACELET = 3,
        ARMOR_TYPE_SHOES = 4,
        ARMOR_TYPE_NECKLAGE = 5,
        ARMOR_TYPE_EARRING = 6,
    };

    enum CostumeTypes {
        COSTUME_TYPE_BODY = 0,
        COSTUME_TYPE_HAIR = 1,
    };

    enum WearFlags {
        WEAR_FLAG_NONE = 0,
        WEAR_FLAG_BODY = 1,
        WEAR_FLAG_HEAD = 2,
        WEAR_FLAG_SHOES = 4,
        WEAR_FLAG_BRACELETS = 8,
        WEAR_FLAG_WEAPON = 16,
        WEAR_FLAG_NECKLACE = 32,
        WEAR_FLAG_EARRING = 64,
        WEAR_FLAG_SHIELD = 256,
        WEAR_FLAG_ARROW = 512,
    };

    enum AntiFlags {
        ANTI_FLAG_FEMALE = 1,
        ANTI_FLAG_MALE = 2,
        ANTI_FLAG_WARRIOR = 4,
        ANTI_FLAG_NINJA = 8,
        ANTI_FLAG_SURA = 16,
        ANTI_FLAG_SHAMAN = 32,
        ANTI_FLAG_SHINSOO = 512,
        ANTI_FLAG_CHUNJO = 1024,
        ANT_FLAG_JINNO = 2048,
        ANTI_FLAG_LYCAN = 262145,
    };

    enum LimitFlags {
        LIMIT_FLAG_LEVEL = 1,
    };

    class Item {
        friend ItemManager;

       public:
        Item(std::shared_ptr<ItemManager> manager, formats::Item proto, uint32_t owner, uint64_t id, uint8_t count = 1);
        virtual ~Item();

        /// Send this item as a set command to the client
        void Send(const std::shared_ptr<core::networking::Connection> &connection) const;
        void SendRemove(const std::shared_ptr<core::networking::Connection> &connection) const;
        static void SendRemove(uint8_t window, uint16_t position,
                               const std::shared_ptr<core::networking::Connection> &connection);

        /// Increase the count and returns the new count.
        uint8_t Add(uint8_t count);

        /// Decrease the count and returns the new count. If the returned count
        /// is zero the item got removed
        uint8_t Remove(uint8_t count);

        /// Removes the item
        void Destroy();

        /// Saves the item to the redis cache
        /// \param create Currently unused but should only be used by
        /// ItemManager!
        void Persist(bool create = false);

        [[nodiscard]] uint64_t GetId() const { return _id; }
        [[nodiscard]] const formats::Item &GetProto() const { return _proto; }
        [[nodiscard]] uint16_t GetPosition() const { return _position; }
        void SetPosition(uint16_t position) { _position = position; }
        [[nodiscard]] uint16_t GetWindow() const { return _window; }
        void SetWindow(uint16_t window) { _window = window; }

       private:
        static void Load(std::shared_ptr<ItemManager> manager, uint64_t id,
                         const std::function<void(std::shared_ptr<Item>)> &cb);

       private:
        std::shared_ptr<ItemManager> _manager;
        formats::Item _proto;

        uint32_t _owner;
        uint64_t _id;
        uint8_t _count;
        uint16_t _window{};
        uint16_t _position{};
    };
}  // namespace game::item
