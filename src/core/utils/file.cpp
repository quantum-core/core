// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "file.hpp"

namespace core::utils {
    std::vector<uint8_t> ReadFile(const std::string &file) {
        std::ifstream ifstream(file, std::ios::in | std::ios::binary);
        return std::vector<uint8_t>(std::istreambuf_iterator<char>{ifstream},
                                    {});
    }
}  // namespace core::utils