// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "player_cache.hpp"

#include <random>

#include "../logger.hpp"
#include "../mysql/migrations/_defaults.hpp"

namespace core::cache {
    PlayerCache::PlayerCache(std::shared_ptr<Redis> redis, std::shared_ptr<mysql::MySQL> database)
        : _redis(std::move(redis)), _database(std::move(database)), _randomDevice(), _random(_randomDevice()) {}

    PlayerCache::~PlayerCache() = default;

    void PlayerCache::GetPlayer(uint32_t playerId, const std::function<void(Player)> &cb) {
        if (playerId == 0) {
            cb({});
            return;
        }

        auto key = "player:" + std::to_string(playerId);
        auto exists = _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});
        if (exists == 0) {
            // Player isn't cache, populate the cache
            CORE_LOGGING(trace) << "Player not cached";

            auto stmt = _database->CreateStatement(
            "SELECT "
                "(SELECT `vnum` FROM `" GAME_DATABASE "`.`items` WHERE `player_id`=`players`.`id` AND `window`=%4% AND `position`=%1%) as `body`, "
                "(SELECT `vnum` FROM `" GAME_DATABASE "`.`items` WHERE `player_id`=`players`.`id` AND `window`=%4% AND `position`=%2%) as `hair`, "
                "(SELECT `vnum` FROM `" GAME_DATABASE "`.`items` WHERE `player_id`=`players`.`id` AND `window`=%4% AND `position`=%3%) as `costume`, "
            "`players`.*, `player_data`.* FROM `" GAME_DATABASE
            "`.`players` LEFT JOIN `" GAME_DATABASE
            "`.`player_data` ON `players`.`id` = `player_data`.`player_id`"
            "WHERE `players`.`id`=%5%");

            stmt << EQUIP_BODY;
            stmt << EQUIP_HAIR;
            stmt << EQUIP_COSTUME;
            stmt << WINDOW_EQUIPMENT;
            stmt << playerId;

            stmt.Execute([this, cb, key, playerId](
                             const core::mysql::MySQLError &error, std::shared_ptr<core::mysql::ResultSet> result) {
                Player player{};
                if (result->Next()) {
                    player.id = playerId;
                    auto name = result->GetString("name");
                    player.name = name;
                    auto classId = result->GetUnsigned8("class");
                    player.playerClass = classId;
                    auto skillGroup = result->GetUnsigned8("skill_group");
                    player.skillGroup = skillGroup;
                    auto x = result->GetUnsigned32("x");
                    player.posX = x;
                    auto y = result->GetUnsigned32("y");
                    player.posY = y;
                    auto hp = result->GetUnsigned32("hp");
                    player.hp = hp;
                    auto mp = result->GetUnsigned32("mp");
                    player.mp = mp;
                    auto stamina = result->GetUnsigned16("stamina");
                    player.stamina = stamina;
                    auto st = result->GetUnsigned16("st");
                    player.st = st;
                    auto ht = result->GetUnsigned16("ht");
                    player.ht = ht;
                    auto dx = result->GetUnsigned16("dx");
                    player.dx = dx;
                    auto iq = result->GetUnsigned16("iq");
                    player.iq = iq;
                    auto level = result->GetUnsigned16("level");
                    player.level = level;
                    auto exp = result->GetUnsigned64("exp");
                    player.exp = exp;
                    auto gold = result->GetUnsigned64("gold");
                    player.gold = gold;
                    auto playtime = result->GetUnsigned64("playtime");
                    player.playtime = playtime;
                    auto body = 0;
                    if (!result->GetIsNull("body")) body = result->GetUnsigned32("body");
                    auto costume = 0;
                    if (!result->GetIsNull("costume")) costume = result->GetUnsigned32("costume");
                    player.body = costume > 0 ? costume : body;
                    auto hair = 0;
                    if (!result->GetIsNull("hair")) hair = result->GetUnsigned32("hair");
                    player.hair = hair;

                    // Write result into cache
                    _redis->Execute(bredis::command_container_t{
                        bredis::single_command_t{"hset", key, "id", std::to_string(playerId)},
                        bredis::single_command_t{"hset", key, "name", name},
                        bredis::single_command_t{"hset", key, "class", std::to_string(classId)},
                        bredis::single_command_t{"hset", key, "skillGroup", std::to_string(skillGroup)},
                        bredis::single_command_t{"hset", key, "x", std::to_string(x)},
                        bredis::single_command_t{"hset", key, "y", std::to_string(y)},
                        bredis::single_command_t{"hset", key, "hp", std::to_string(hp)},
                        bredis::single_command_t{"hset", key, "mp", std::to_string(mp)},
                        bredis::single_command_t{"hset", key, "stamina", std::to_string(stamina)},
                        bredis::single_command_t{"hset", key, "st", std::to_string(st)},
                        bredis::single_command_t{"hset", key, "ht", std::to_string(ht)},
                        bredis::single_command_t{"hset", key, "dx", std::to_string(dx)},
                        bredis::single_command_t{"hset", key, "iq", std::to_string(iq)},
                        bredis::single_command_t{"hset", key, "level", std::to_string(level)},
                        bredis::single_command_t{"hset", key, "exp", std::to_string(exp)},
                        bredis::single_command_t{"hset", key, "gold", std::to_string(gold)},
                        bredis::single_command_t{"hset", key, "playtime", std::to_string(playtime)},
                        bredis::single_command_t{"hset", key, "body", std::to_string(player.body)},
                        bredis::single_command_t{"hset", key, "hair", std::to_string(hair)}});
                } else {
                    // Player id is invalid, this should never happen if the
                    // database is correct
                    CORE_LOGGING(error) << "Player not found!";
                }
                cb(player);
            });

            return;
        }

        Player player{};

        // Read all keys from cache
        auto result = _redis->ExecuteArray(bredis::single_command_t{"hgetall", key});
        for (auto i = 0; i < result.elements.size(); i += 2) {
            auto fieldName = boost::get<string_t>(result.elements[i]);
            auto value = boost::get<string_t>(result.elements[i + 1]);

            if (fieldName.str == "id") {
                player.id = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "name") {
                player.name = value.str;
            } else if (fieldName.str == "class") {
                player.playerClass = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "skillGroup") {
                player.skillGroup = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "x") {
                player.posX = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "y") {
                player.posY = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "hp") {
                player.hp = boost::lexical_cast<int64_t>(value.str);
            } else if (fieldName.str == "mp") {
                player.mp = boost::lexical_cast<int64_t>(value.str);
            } else if (fieldName.str == "stamina") {
                player.stamina = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "st") {
                player.st = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "ht") {
                player.ht = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "dx") {
                player.dx = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "iq") {
                player.iq = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "level") {
                player.level = static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
            } else if (fieldName.str == "exp") {
                player.exp = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "gold") {
                player.gold = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "playtime") {
                player.playtime = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "teleportationX") {
                player.teleportationX = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "teleportationY") {
                player.teleportationY = boost::lexical_cast<int32_t>(value.str);
            } else if (fieldName.str == "body") {
                player.body = boost::lexical_cast<uint32_t>(value.str);
            } else if (fieldName.str == "hair") {
                player.hair = boost::lexical_cast<uint32_t>(value.str);
            } else {
                CORE_LOGGING(trace) << "Unknown cache field for player " << fieldName.str << " (value: " << value.str
                                    << ")";
            }
        }

        cb(player);
    }

    void PlayerCache::AddPlayer(uint32_t accountId, uint32_t playerId, uint8_t slot) {
        auto key = "players:" + std::to_string(accountId);
        CORE_LOGGING(trace) << "Adding player " << playerId << " to " << accountId;

        _redis->Execute(bredis::single_command_t{"hset", key, std::to_string(slot), std::to_string(playerId)});
    }

    void PlayerCache::GetPlayers(uint32_t accountId, const std::function<void(const std::vector<uint32_t> &)> &cb) {
        auto key = "players:" + std::to_string(accountId);
        CORE_LOGGING(trace) << "Reading player cache for account " << accountId;

        // Check if key exists
        auto res = _redis->Execute(bredis::single_command_t{"exists", key});
        if (auto exists = boost::get<int64_t>(&res)) {
            if (*exists == 1) {
                std::vector<uint32_t> ret;
                // Key exists so we can read from the cache here
                CORE_LOGGING(trace) << "Cache exists";

                for (auto i = 0; i < 4; i++) {
                    res = _redis->Execute(bredis::single_command_t{"hget", key, std::to_string(i)});
                    auto str = boost::get<string_t>(&res);
                    if (!str) {
                        CORE_LOGGING(trace) << "No entry in position " << i;
                        ret.push_back(0);
                        continue;
                    }

                    auto playerId = boost::lexical_cast<uint32_t>(str->str);

                    CORE_LOGGING(trace) << "Position " << i << " Player: " << playerId;
                    ret.push_back(playerId);
                }

                cb(ret);
                return;
            }
        }

        // We don't have any cache key, we'll load from the database and
        // populate the cache
        auto stmt = _database->CreateStatement("SELECT `id`, `slot` FROM `" GAME_DATABASE
                                               "`.`players` WHERE `account_id` = %1% ORDER BY `slot` ASC");
        stmt << accountId;
        stmt.Execute(
            [this, cb, key](const core::mysql::MySQLError &error, std::shared_ptr<core::mysql::ResultSet> result) {
                std::vector<uint32_t> ret;
                while (result->Next()) {
                    auto playerId = result->GetUnsigned32("id");
                    auto slotIdx = result->GetUnsigned8("slot");
                    if (ret.size() == slotIdx) {
                        ret.push_back(playerId);
                    } else {
                        for (auto i = ret.size(); i < slotIdx; i++) {
                            ret.push_back(0);
                        }
                        ret.push_back(playerId);
                    }

                    _redis->Execute(
                        bredis::single_command_t{"hset", key, std::to_string(slotIdx), std::to_string(playerId)});
                }
                cb(ret);
            });
    }

    uint32_t PlayerCache::GenerateLoginKey(uint32_t accountId) {
        std::uniform_int_distribution<uint32_t> dist;

        bool unique = false;
        uint32_t id;
        while (!unique) {
            CORE_LOGGING(trace) << "Generating login key";

            auto random = dist(_random);
            auto key = "login:" + std::to_string(random);
            auto exists = _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});

            unique = exists == 0;
            CORE_LOGGING(trace) << "Key " << random << " is unique? " << unique;
            id = random;

            if (unique) {
                _redis->Execute(bredis::single_command_t{"set", key, std::to_string(accountId)});
            }
        }

        return id;
    }

    uint32_t PlayerCache::CheckLoginKey(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        auto exists = _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});

        if (exists != 1) return 0;

        auto accountId = _redis->Execute<string_t>(bredis::single_command_t{"get", key});
        return boost::lexical_cast<uint32_t>(accountId.str);
    }

    void PlayerCache::SetLoginKeyTTL(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        _redis->Execute(bredis::single_command_t{"expire", key, "30"});  // 30 seconds life time of login key
    }

    void PlayerCache::RemoveLoginKeyTTL(uint32_t loginKey) {
        auto key = "login:" + std::to_string(loginKey);
        _redis->Execute(bredis::single_command_t{"persist", key});
    }

    void PlayerCache::SavePlayer(const Player &player) {
        auto key = "player:" + std::to_string(player.id);
        _redis->Execute(bredis::command_container_t{
            bredis::single_command_t{"hset", key, "name", player.name},
            bredis::single_command_t{"hset", key, "class", std::to_string(player.playerClass)},
            bredis::single_command_t{"hset", key, "skillGroup", std::to_string(player.skillGroup)},
            bredis::single_command_t{"hset", key, "x", std::to_string(player.posX)},
            bredis::single_command_t{"hset", key, "y", std::to_string(player.posY)},
            bredis::single_command_t{"hset", key, "teleportationX", std::to_string(player.teleportationX)},
            bredis::single_command_t{"hset", key, "teleportationY", std::to_string(player.teleportationY)},
            bredis::single_command_t{"hset", key, "hp", std::to_string(player.hp)},
            bredis::single_command_t{"hset", key, "mp", std::to_string(player.mp)},
            bredis::single_command_t{"hset", key, "stamina", std::to_string(player.stamina)},
            bredis::single_command_t{"hset", key, "st", std::to_string(player.st)},
            bredis::single_command_t{"hset", key, "ht", std::to_string(player.ht)},
            bredis::single_command_t{"hset", key, "dx", std::to_string(player.dx)},
            bredis::single_command_t{"hset", key, "iq", std::to_string(player.iq)},
            bredis::single_command_t{"hset", key, "level", std::to_string(player.level)},
            bredis::single_command_t{"hset", key, "exp", std::to_string(player.exp)},
            bredis::single_command_t{"hset", key, "gold", std::to_string(player.gold)},
            bredis::single_command_t{"hset", key, "playtime", std::to_string(player.playtime)},
            bredis::single_command_t{"hset", key, "body", std::to_string(player.body)},
            bredis::single_command_t{"hset", key, "hair", std::to_string(player.hair)},
        });
    }

    void PlayerCache::SetTeleportationPosition(uint32_t playerId, int32_t x, int32_t y) {
        auto key = "player:" + std::to_string(playerId);
        _redis->Execute(bredis::command_container_t{
            bredis::single_command_t{"hset", key, "teleportationX", std::to_string(x)},
            bredis::single_command_t{"hset", key, "teleportationY", std::to_string(y)},
        });
    }

}  // namespace core::cache