// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "event_system.hpp"

#include "event.hpp"

namespace core::event {
    EventSystem::EventSystem() = default;
    EventSystem::~EventSystem() = default;

    void EventSystem::Update(unsigned int elapsed) {
        // Process create and cancel events
        ProcessQueues();

        for (const auto& event : _events) {
            if (event.second->DecrementTimeout(elapsed)) {
                if (event.second->Execute()) {
                    // Remove this event
                    CancelEvent(event.first);
                }
            }
        }
    }

    void EventSystem::ProcessQueues() {
        std::shared_ptr<Event> evt;
        while (_createQueue.try_dequeue(evt)) {
            _events[evt->GetId()] = evt;
        }

        unsigned long eventId;
        while (_cancelQueue.try_dequeue(eventId)) {
            _events.erase(eventId);
        }
    }

    std::weak_ptr<Event> EventSystem::EnqueueEvent(const std::function<int()>& function, int timeout) {
        auto event = std::make_shared<Event>(_eventIdCounter++, function, timeout);
        _createQueue.enqueue(event);

        return event;
    }

    void EventSystem::CancelEvent(unsigned long id) { _cancelQueue.enqueue(id); }

    void EventSystem::CancelEvent(const std::weak_ptr<Event>& event) {
        auto evt = event.lock();
        if (evt) {
            CancelEvent(evt->GetId());
        }
    }
}  // namespace core::event