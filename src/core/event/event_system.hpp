// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <concurrentqueue.h>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <unordered_map>

namespace core::event {
    class Event;

    class EventSystem {
       public:
        EventSystem();
        virtual ~EventSystem();

        /**
         * Will update all queue events.
         * This method is not thread safe and should only getting called from one thread at a time.
         * @param elapsed Time since last update in milliseconds.
         */
        void Update(unsigned int elapsed);

        /**
         * Will enqueue a functions, the function will get called from the update thread.
         * @param function The function which will get called. The return value of the function is the time in
         * milliseconds until the function will get called again. If the method returns zero the event will get
         * cancelled.
         * @param timeout Initial timeout in milliseconds, if the timeout is zero the event will called on the next
         * update.
         * @return A weak reference to the created event.
         */
        std::weak_ptr<Event> EnqueueEvent(const std::function<int()> &function, int timeout);

        /**
         * Cancel the given event. If the event doesn't exists the method will have no effect.
         * @param id The unique id of the event.
         */
        void CancelEvent(unsigned long id);

        /**
         * Cancel the given event. If the event doesn't exists the method will have no effect.
         * @param event The reference to the event. If the reference is invalid this call will have no effect.
         */
        void CancelEvent(const std::weak_ptr<Event> &event);

       private:
        void ProcessQueues();

       private:
        std::atomic<unsigned long> _eventIdCounter{0};
        std::unordered_map<unsigned long, std::shared_ptr<Event>> _events;

        moodycamel::ConcurrentQueue<std::shared_ptr<Event>> _createQueue;
        moodycamel::ConcurrentQueue<unsigned long> _cancelQueue;
    };
}  // namespace core::event
