// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include "../structures/table.hpp"
#include "action.hpp"

namespace core::mysql::actions {
    class CreateTable : public Action {
       public:
        explicit CreateTable(const structures::Table &table);

        std::string ToSQL() override;

       private:
        structures::Table _table;
    };
}  // namespace core::mysql::actions