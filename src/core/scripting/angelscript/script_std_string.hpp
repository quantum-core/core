// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#ifdef ENABLE_ANGELSCRIPT

#include <angelscript.h>

// Sometimes it may be desired to use the same method names as used by C++ STL.
// This may for example reduce time when converting code from script to C++ or
// back.
//
//  0 = off
//  1 = on

#ifndef AS_USE_STLNAMES
#define AS_USE_STLNAMES 0
#endif

namespace core::scripting::angelscript {
    void RegisterStdString(asIScriptEngine *engine);
    void RegisterStdStringUtils(asIScriptEngine *engine);
}  // namespace core::scripting::angelscript

#endif
